FangKuai=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                         for i,v in pairs(agr) do
                            obj[i] = v
                         end
                    end
                end )
--初始化方块
function FangKuai:new(i,j)
	--方块的分数
	self.fenShu = 0
	--方块所在的行
	self.X = i
	--方块所在的列
	self.Y = j
	--方块是否正在被拖动
	self.moveing = false
	--方块杯拖动前的坐标
	self.beginPos = ccp(0,0)
	--即将被融合调的对象
	self.rongHeFK = nil
	--结算时,方块的积分
	self.jieSuanFen = 0
	--1分或者2分得时候,重叠需要的白色方块
	self.baiKuang = nil
end

function FangKuai:create()
	--初始化CCB
	self.layer = CCBuilderReaderLoad("FangKuai.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
	self.layer:setPosition(ccp(0,0))
	gameLayer.kuangZeroNode:addChild(self.layer)

	--根据精灵的积分,选择卡片的背景
	if self.fenShu == 0 then
		return 0
	end
	if self.fenShu == 1 then
		self.sp:setTexture(CCSprite:create("1.png"):getTexture())
	end
	if self.fenShu == 2 then
		self.sp:setTexture(CCSprite:create("2.png"):getTexture())
	end
	if self.fenShu >= 3 then
		self.sp:setTexture(CCSprite:create("3.png"):getTexture())
	end
	--设置数字的大小
	self:setTTFSize()

	--给卡片设置出场效果,并添加
	self.beginPos = ccp(OriginX + spSize.width * self.Y,OriginY + spSize.height * self.X)
	local fangXiang = fangKuaiManager.moveFangXiang
	if fangXiang ~= "" then
		if fangXiang == "shang" then
			self.sp:setPosition(self.beginPos.x,-spSize.height)
		end
		if fangXiang == "xia" then
			self.sp:setPosition(self.beginPos.x,winSize.height+spSize.height)
		end
		if fangXiang == "left" then
			self.sp:setPosition(winSize.width+spSize.width,self.beginPos.y)
		end
		if fangXiang == "right" then
			self.sp:setPosition(-spSize.width,self.beginPos.y)
		end
		local move = CCMoveTo:create(0.05,self.beginPos)
		self.sp:runAction(move)
	else
		self.sp:setPosition(self.beginPos)
	end
	self.ttf:setPosition(ccp(spSize.width*0.5,spSize.height*0.4))

	--根据积分修改卡片上的数字
	if self.fenShu >= 3 then
		self.ttf:setString(self.fenShu)
	else
		self.ttf:setString("")
	end
end

--根据现在的分数,对方块进行更新
function FangKuai:updateSp()
	--如果正好是3,肯定是1和2合成的,修改成白色
	if self.fenShu >= 3 then
		self.sp:setTexture(CCSprite:create("3.png"):getTexture())
	end

	--设置数字的大小
	self:setTTFSize()
	--删除可能生成的白色边框
	self:deleteBaiKuang()
	--检测是否刷新本局的最高分
	self:topFenUpdate()

	--检测是否成为了历史最高分方块
	if self.fenShu > gameManager.topFKFen then
		gameManager.topFKFen = self.fenShu
		HightFenTanKuang:createLayer()
		gameManager:shuaXinZuiGaoFKFen()
	end
end

--设置数字的大小
function FangKuai:setTTFSize()
	if self.fenShu >= 3 then
		--如果大于3,代码添加数字
		--根据位数
		local weiShu = math.floor(math.log10(self.fenShu) + 1)
		if weiShu == 1 then
			self.ttf:setFontSize(140)
		elseif weiShu == 2 then
			self.ttf:setFontSize(88)
			self.ttf:setPosition(ccp(spSize.width*0.5 , spSize.height*0.45))
		elseif weiShu == 3 then
			self.ttf:setFontSize(55)
			self.ttf:setPosition(ccp(spSize.width*0.5 , spSize.height*0.5))
		elseif weiShu == 4 then
			self.ttf:setFontSize(44)
			self.ttf:setPosition(ccp(spSize.width*0.5 , spSize.height*0.48))
		else
			self.ttf:setFontSize(33)
			self.ttf:setPosition(ccp(spSize.width*0.5 , spSize.height*0.5))
		end

		self.ttf:setString(self.fenShu)
	else
		--如果小于3,不需要写数字
		self.ttf:setString("")
	end
end

function FangKuai:deleteBaiKuang()
	--删除可能生成的融合动画用的白色方块
	if self.baiKuang ~= nil then
		--cclog("fk.X = %d , fk.Y = %d",fk.X,fk.Y)
		self.baiKuang:removeFromParentAndCleanup(true)
		self.baiKuang = nil
	end
end

--消除方块
function FangKuai:deleteSelf()
	--方块的精灵
	self.layer:removeFromParentAndCleanup(true)
end

function FangKuai:move()
    local move_ease = CCEaseSineInOut:create(CCMoveBy:create(3, 0, 0))
    local move_ease_back = move_ease:reverse()

	local arr = CCArray:create()
	arr:addObject(move_ease)
	arr:addObject( CCDelayTime:create(0.25))
	arr:addObject(move_ease_back)
	arr:addObject( CCDelayTime:create(0.25))
    local seq = CCSequence:create(arr)

    tamara:runAction(CCRepeatForever:create(seq))
end

--此方法在CCB的动画里调用
function FangKuai:jieSuanFunc()
	
	if self.fenShu == 3 then
		playEffect("NewMerge1.mp3")
	elseif  self.fenShu == 6 then
		playEffect("NewMerge2.mp3")
		elseif  self.fenShu == 12 then
		playEffect("NewMerge3.mp3")
		elseif  self.fenShu == 24 then
		playEffect("NewMerge4.mp3")
		elseif  self.fenShu == 48 then
		playEffect("NewMerge5.mp3")
		elseif  self.fenShu == 96 then
		playEffect("NewMerge6.mp3")
		elseif  self.fenShu == 96*2 then
		playEffect("NewMerge7.mp3")
		elseif  self.fenShu == 96*2*2 then
		playEffect("NewMerge8.mp3")
		elseif  self.fenShu == 96*2*2*2 then
		playEffect("NewMerge9.mp3")
	end


	if self.fenShu == 1 then
		self.sp:setTexture(CCSprite:create("hui1.png"):getTexture())
	elseif self.fenShu == 2 then
		self.sp:setTexture(CCSprite:create("hui2.png"):getTexture())
	else
		self.jieSuanFen = math.pow(3, math.log(self.fenShu / 3 , 2) ) * 3
		self.jieSuanFenTTF:setString("+"..self.jieSuanFen)

		fangKuaiManager.endGameJiFen = fangKuaiManager.endGameJiFen + self.jieSuanFen
		gameLayer.jieSuanJiFenTTF:setString(fangKuaiManager.endGameJiFen)
	end

	--检测是否在游戏结束时需要播放庆祝
	if fangKuaiManager.liZiQingZhuOnEnd == false then
		if fangKuaiManager.endGameJiFen > gameManager.topFen then
			fangKuaiManager.liZiQingZhuOnEnd = true
			gameManager:shuaXinZuiGaoFen()
		end
	end
end

--检测本局的最高分是否被刷新
function FangKuai:topFenUpdate()
	local fenShu = self.fenShu
	local topFen = fangKuaiManager.benJuTopFen
	if fenShu > topFen then
		--突破了最高分
		fangKuaiManager.benJuTopFen = fenShu
		for i=1,HANG do
			for j=1,LIE do
				local fk1 = fangKuaiManager.fangKuaiTable[i][j]
				if fk1.ttf ~= nil then
					fk1.ttf:setColor(ccc3(0,0,0))
				end
			end
		end
		self.ttf:setColor(ccc3(255,0,0))
	elseif fenShu == topFen then
		--达到了最高分
		self.ttf:setColor(ccc3(255,0,0))
	end
end