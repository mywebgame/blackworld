--游戏总体控制
	--游戏区域的行数
	HANG = 4
	--游戏区域的列数
	LIE = 4
	--跳转场景的时候,场景移动需要的时间
	TIAOZHUANCHANGJINGTIME = 0.5

--BeginLayer相关控制
	--游戏记录最大的保存数量
	JILUBAOCUNSHULIANG = 10
	--BeginLayer出现的时候停止在哪个空间上
	BEGINLAYERTINGZHIKONGJIAN = 3

--开局刷新控制
	--每局开始的时候,三种分数刷新数量的保底
	SHULIANGBAODI = 2
	--每局开始的时候,三种分数刷新数量的上限
	SHULIANGSHANGXIAN = 4
	--每局开始的时候,生成方块的总数
	MANGESHULIANG = 9

--方块融合控制
	--方块重叠超过时,开始重合
	RONGHEBI = 0.3
	--拖动后不是刚好吻合时,自动对准重合的移动速度
	XIAOZHUNRONGHESPEED = 300

--游戏结算控制
	--游戏结算时,揭开一个方块后揭开下一个方块需要等待的时间
	ENDGAMEFANGKUAIYANSHI = 1/2
	--游戏结算时,点击一次屏幕加速开牌的速度
	ENDGAMECLICKJIASU = 0.2
	--游戏结算时,点击加速的上限
	ENDGAMECLICKJIASUSHANGXIAN = 3
	--游戏结算时,输入姓名时可输入的最长字符数
	ENDGAMENAMEZIFUSHU = 12
	--结算时如果输入框为空,则默认使用的名字
	NAMELAYERMORENNAME = "Threes"
	--名字输入完毕,等待多久名字开始开始飞
	NAMEENDDENGDAI = 0.5

--随机生成控制
	--蓝色方块的生成概率
	MAKELANGAILV = 1/4
	--红色方块的生成概率
	MAKEHONGGAILV = 1/4
	--非白色方块的概率(如果红色和蓝色不足这个概率,会必定生成较少的)
	MAKEFEIBAIGAILV = 3/4
	--其余都为白色,白色无需设置
	--白色方块中出现3的概率,其余部分随机平均分配
	MAKEBAI3GAILV = 1/2

--拖动控制
	--拖动方块时,未被拖动的方块的拉伸度
	TUODONGSHIWEIDONGLASHENBILI = 0.05

--不可更改的游戏控制
	--在记录右边有多少个全屏的可拖动控件
	JILUYOUBIANDEKONGJIANSHU = 2