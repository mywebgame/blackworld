FangKuaiManager=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                         for i,v in pairs(agr) do
                            obj[i] = v
                         end
                    end
                end )

function FangKuaiManager:new()
	fangKuaiManager = self
	--方块Table
	self.fangKuaiTable = {}
	--当前拖动时的方向
	self.moveFangXiang = ""
	--这个属性用来控制移动时是否已经判断出移动方向
	self.isMoveing = false
	--当前游戏最高分的方块分数,用来生成下一个方块
	self.topFen = 3
	--下一个方块的分数
	self.nextFenShu = 0
	--本次touth事件是否成功移动
	self.isTouthYes = false
	--拖动方块后,方块滑动所需的时间
	self.huaDongTime = 0
	--游戏是否已经执行过结算
	self.isJieSuan = false
	--游戏结束的时候,结算动画播放的延迟
	self.endGameFangKuaiYanChi = 0
	--游戏结束的时候,显示的最终积分
	self.endGameJiFen = 0
	--游戏结束时,玩家留下的名字
	self.endGameName = nil
	--游戏结束时,游戏截图的文件名
	self.jieTuName = ""
	--本局游戏出现的最高分
	self.benJuTopFen = 6
	--游戏结束时,是否已经开始播放突破最高分的粒子
	self.liZiQingZhuOnEnd = false
	--游戏结束时,翻拍的速度(倍数)
	self.gameEndFanPaiSpeed = 1
end

--这个方法根据传入的数字给方块随机分配一个位置
function FangKuaiManager:kaPianSuiJiWeiZhi(tempfenShu)
	local weiZhi = math.floor( math.random() * HANG * LIE + 1)
	local X = math.ceil(weiZhi/LIE)
	local Y = weiZhi - (X-1)*LIE

	if self.fangKuaiTable[X][Y].fenShu == 0 then
		self.fangKuaiTable[X][Y].fenShu = tempfenShu
	else
		self:kaPianSuiJiWeiZhi(tempfenShu)
	end
end

function FangKuaiManager:suiJiChuShiHuaFangKuai()
	for i=1,HANG do
		self.fangKuaiTable[i] = {}
		for j=1,LIE do
			self.fangKuaiTable[i][j] = FangKuai()
			self.fangKuaiTable[i][j]:new(i,j)
		end
	end

	--第一轮保底生成
		for i=1,SHULIANGBAODI do
			self:kaPianSuiJiWeiZhi(1)
			self:kaPianSuiJiWeiZhi(2)
			self:kaPianSuiJiWeiZhi(3)
		end

	--第二轮随机生成
		local lan = SHULIANGBAODI
		local hong = SHULIANGBAODI
		local bai = SHULIANGBAODI

		local function kaPaiSuiJiGaiLv()
			local suiji = math.random()
			if suiji < 1/3 then
				if lan < SHULIANGSHANGXIAN then
					lan = lan +1
					self:kaPianSuiJiWeiZhi(1)
				else
					kaPaiSuiJiGaiLv()
				end
			elseif suiji < 2/3 then
				if hong < SHULIANGSHANGXIAN then
					hong = hong +1
					self:kaPianSuiJiWeiZhi(2)
				else
					kaPaiSuiJiGaiLv()
				end
			else
				if bai < SHULIANGSHANGXIAN then
					bai = bai +1
					self:kaPianSuiJiWeiZhi(3)
				else
					kaPaiSuiJiGaiLv()
				end
			end
		end

		for i=1,MANGESHULIANG-SHULIANGBAODI*3 do
			kaPaiSuiJiGaiLv()
		end

	--这里根据当前方块的位置,进行生成
	for i=1,HANG do
		for j=1,LIE do
			local fk = self.fangKuaiTable[i][j]
			if fk.fenShu ~= 0 then
				--这个位置有方块,生成方块
				fk:create(gameLayer.kuangZeroNode)
			end
		end
	end
end