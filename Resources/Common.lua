DEBUG_MODE = true
OPEN_CTDUMP = true
cclog = function(...)
	--if false then
	    print(string.format(...))
	--end
end

--输出table变量至io
local function serialize (file, o)
	if type(o) == "number" then
    	file:write(o)
   	elseif type(o) == "string" then
       	file:write(string.format("%q", o))
  	elseif type(o) == "table" then
       	file:write("{\n")
       	for k,v in pairs(o) do
       		if type(k) ~= "number" then
          		file:write(" ", k, " = ")
          	end
          	serialize(file, v)
          	file:write(",\n")
		end
       	file:write("}\n")
	elseif type(o) == "booMaskedSpritelean" then
		if o then 
			file:write("true")
		else
			file:write("false")
		end
   	else
       	error("cannot serialize a " .. type(o))
   	end
end

--输出table变量至io
local function serializeForString (tableName, o)
	local fileString = ""
	if tableName then
		fileString = tableName.."="
	end

	if type(o) == "number" then
    	fileString =  string.format('%s %d', fileString, o)
   	elseif type(o) == "string" then
       	fileString = string.format("%s %q", fileString, o)
  	elseif type(o) == "table" then
       	fileString = string.format("%s{\n", fileString)
       	for k,v in pairs(o) do
       		if type(k) ~= "number" then
          		fileString = string.format("%s %s=%s,\n", fileString, tostring(k), serializeForString(nil, v))
          	else
          		fileString = string.format("%s %s,\n", fileString, serializeForString(nil, v))
          	end
		end
       	fileString = string.format("%s}\n", fileString)
	elseif type(o) == "boolean" then
		if o then 
     	  	fileString = string.format("%s true", fileString)
		else
     	  	fileString = string.format("%s false", fileString)
		end
   	else
   		GF_trace("error_when_serialize")
       	error("cannot serialize a " .. type(o))
   	end

   	return fileString
end

--[[------------------------------------------------------------------
	保存文件
	table		要保存的表
	tableName	保存文件中bable变量的名称，若不指定，则读出的文件无法解析
	fileName	要保存至的文件
--]]------------------------------------------------------------------
function GF_saveTableToFileForString(table, tableName, fileName)
    local tableString = serializeForString(tableName, table)
    RedCommonUtils:encryptStr2File(tableString, fileName)
end


function GF_saveTableToFile(table, tableName, fileName)
	--GF_saveTableToFileForString(table, tableName, fileName..'d')
	--GF_dump(table)
	local function file_exists( path )
		local file = io.open(path, "rb") 
		if file then 
			file:close()
		end
		return file ~= nil
	end
	   assert(fileName~=nil, "saveVarToFile function need fileName")
	   local file = io.open(fileName, "w")
	   -- CCLOG("saving table to "..fileName)
	   -- CCLOG(type(file))
	--变量名名称为必须，否则写入的文件无法读出
	if tableName ~= nil then
		file:write(tableName.." = ")
	else
		error("saveVarToFile function need varName")
	end
	--之后文件以添加的方式写入
	io.close(file)
	file = io.open(fileName, "a")
	serialize(file, table)
	io.close(file)
end

--打印某对象的值
GF_dump = function(object, label, nesting, nest)
	if DEBUG_MODE and OPEN_CTDUMP then
		if type(nesting) ~= "number" then nesting = 99 end
	    local lookup_table = {}
	    local function _dump(object, label, indent, nest)
	        label = label or "<var>"
	        if type(object) ~= "table" then
	            print(string.format("%s%s = %s", indent, tostring(label), tostring(object)..""))
	        elseif lookup_table[object] then
	            print(string.format("%s%s = *REF*", indent, tostring(label)))
	        else
	            lookup_table[object] = true
	            if nest > nesting then
	                print(string.format("%s%s = *MAX NESTING*", indent, label))
	            else
	                print(string.format("%s%s = {", indent, tostring(label)))
	                local indent2 = indent.."    "
	                for k, v in pairs(object) do
	                    _dump(v, k, indent2, nest + 1)
	                end
	                print(string.format("%s}", indent))
	            end
	        end
	    end
	    _dump(object, label, "- ", 1)
	end
end

GF_trace = function(msg)
    if true then
        print("----------------------------------------")
        if msg then
            print("traceback for msg : "..msg)
        end
        print(debug.traceback())
        print("----------------------------------------")
    end
end

--判断文件是否存在
function GF_file_exists(path)
	local file = io.open(path, "rb") 
	if file then 
		file:close()
	end
	return file ~= nil 
	-- return RedCommonUtils:isFileWithPathExists(path)
end

--延时执行某个方法
function GF_performWithDelay(time, listener)
	local scheduler = CCDirector:sharedDirector():getScheduler()
    local handle
    handle = scheduler:scheduleScriptFunc(function()
        scheduler:unscheduleScriptEntry(handle)
        listener()
    end, time, false)
    return handle
end

--延时执行某个方法
function GF_performFuncWithDelay(time, listener, func)
	local scheduler = CCDirector:sharedDirector():getScheduler()
    local handle
    handle = scheduler:scheduleScriptFunc(function()
        scheduler:unscheduleScriptEntry(handle)
        func(listener)
    end, time, false)
    return handle
end


--[[-----------------------------
	动作回调序列
	node 		执行动作的参数
	func 		执行完动作的回调函数
	action		需要执行的动作
--]]-----------------------------------
function WL_dongZuoHuiDiaoHanShu(node,func,action)
	local arr = CCArray:create()
    arr:addObject(action)
	arr:addObject(CCCallFunc:create(listener))
	node:runAction( CCSequence:create(arr))
end