--根据重叠,对重叠进行处理
function FangKuaiManager:baiKuangLaShen(fk)
	local upDatePosX,upDatePosY = fk.sp:getPosition()
	local fangXiang = fangKuaiManager.moveFangXiang
	--重叠变色的方块必须是1或者2
	if fk.fenShu == 1 or fk.fenShu == 2 then
		if fangXiang == "shang" then
			--上下缩放
			local fk1 = fangKuaiManager.fangKuaiTable[fk.X+1][fk.Y]
			--判断是否重叠了
			if fk1.moveing == false and fk1.fenShu ~= 0 then
				--已经重叠了,判断白框是否已经生成
				if fk.baiKuang == nil then
					--白框不存在,创建白框
					fk.baiKuang = CCSprite:create("3.png")
					fk.baiKuang:setAnchorPoint(ccp(0,1))
					fk.baiKuang:setPosition(ccp(0,spSize.height))
					fk.baiKuang:setScaleY(0)
					fk.sp:addChild(fk.baiKuang)
				end
				--进行缩放处理
				local scale = (upDatePosY - fk.beginPos.y) / spSize.height
				fk.baiKuang:setScaleY(scale)
			end
		end

		if fangXiang == "xia" then
			--上下缩放
			local fk1 = fangKuaiManager.fangKuaiTable[fk.X-1][fk.Y]
			--判断是否重叠了
			if fk1.moveing == false and fk1.fenShu ~= 0 then
				--已经重叠了,判断白框是否已经生成
				if fk.baiKuang == nil then
					--白框不存在,创建白框
					fk.baiKuang = CCSprite:create("3.png")
					fk.baiKuang:setAnchorPoint(ccp(0,0))
					fk.baiKuang:setPosition(ccp(0,0))
					fk.baiKuang:setScaleY(0)
					fk.sp:addChild(fk.baiKuang)
				end
				--进行缩放处理
				local scale = (fk.beginPos.y - upDatePosY) / spSize.height
				fk.baiKuang:setScaleY(scale)
			end
		end

		if fangXiang == "left" then
			--上下缩放
			local fk1 = fangKuaiManager.fangKuaiTable[fk.X][fk.Y-1]
			--判断是否重叠了
			if fk1.moveing == false and fk1.fenShu ~= 0 then
				--已经重叠了,判断白框是否已经生成
				if fk.baiKuang == nil then
					--白框不存在,创建白框
					fk.baiKuang = CCSprite:create("3.png")
					fk.baiKuang:setAnchorPoint(ccp(0,0))
					fk.baiKuang:setPosition(ccp(0,0))
					fk.baiKuang:setScaleX(0)
					fk.sp:addChild(fk.baiKuang)
				end
				--进行缩放处理
				local scale = (fk.beginPos.x - upDatePosX) / spSize.width
				fk.baiKuang:setScaleX(scale)
			end
		end

		if fangXiang == "right" then
			--上下缩放
			local fk1 = fangKuaiManager.fangKuaiTable[fk.X][fk.Y+1]
			--判断是否重叠了
			if fk1.moveing == false and fk1.fenShu ~= 0 then
				--已经重叠了,判断白框是否已经生成
				if fk.baiKuang == nil then
					--白框不存在,创建白框
					fk.baiKuang = CCSprite:create("3.png")
					fk.baiKuang:setAnchorPoint(ccp(1,0))
					fk.baiKuang:setPosition(ccp(spSize.width,0))
					fk.baiKuang:setScaleX(0)
					fk.sp:addChild(fk.baiKuang)
				end
				--进行缩放处理
				local scale = (upDatePosX - fk.beginPos.x) / spSize.width
				fk.baiKuang:setScaleX(scale)
			end
		end
	end
end

--松开手之后,白色边框的变化
function FangKuaiManager:baiSeKuangPuMan(fk)
	if fk.baiKuang ~= nil then
		local scale = CCEaseExponentialOut:create(CCScaleTo:create(fangKuaiManager.huaDongTime, 1,1))
		fk.baiKuang:runAction(scale)
	end
end

--没有达到需要的拖动跨度,方块返回,白色边框的变化
function FangKuaiManager:baiSeKuangSuoXiao(fk)
	local fangXiang = fangKuaiManager.moveFangXiang
	if fk.baiKuang ~= nil then
		local scale
		if fangXiang == "shang" or fangXiang == "xia" then
			scale = CCEaseExponentialOut:create(CCScaleTo:create(fangKuaiManager.huaDongTime, 1,0))
		else
			scale = CCEaseExponentialOut:create(CCScaleTo:create(fangKuaiManager.huaDongTime, 0,1))
		end
		local function funC( )
			if fk.baiKuang ~= nil then
				--cclog("fk.X = %d , fk.Y = %d",fk.X,fk.Y)
				fk.baiKuang:removeFromParentAndCleanup(true)
				fk.baiKuang = nil
			end
		end

		local arr = CCArray:create()
		arr:addObject(scale)
		arr:addObject(CCCallFunc:create(funC))
		fk.baiKuang:runAction( CCSequence:create(arr))
	end
end

--根据重叠,对未移动的方块进行微量便宜
function FangKuaiManager:weiDongPianYi(fk)
	local upDatePosX,upDatePosY = fk.sp:getPosition()
	local yPianYi = upDatePosY - fk.beginPos.y
	local xPianYi = upDatePosX - fk.beginPos.x
	for i=1,HANG do
		for j=1,LIE do
			local fk1 = fangKuaiManager.fangKuaiTable[i][j]
			if fk1.moveing == false and fk1.fenShu > 0 then
				fk1.sp:setScaleX(1+ math.abs(xPianYi)/spSize.width*TUODONGSHIWEIDONGLASHENBILI)
				fk1.sp:setScaleY(1+ math.abs(yPianYi)/spSize.height*TUODONGSHIWEIDONGLASHENBILI)
				fk1.sp:setPosition(ccp(fk1.beginPos.x + xPianYi*TUODONGSHIWEIDONGLASHENBILI/2 , 
										fk1.beginPos.y + yPianYi*TUODONGSHIWEIDONGLASHENBILI/2))
			end
		end
	end
end
