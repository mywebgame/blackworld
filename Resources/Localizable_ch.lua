Localizable = {
	XSMoShiStep0_1 = "Threes!\n 我们看到了 1 和 2",
	XSMoShiStep0_2 = "您可以移动他们到任何位置",
	XSMoShiStep0_3 = "把数字推到边框上",
	XSMoShiStep0_4 = "利用边框让 1 和 2 合在一起",

	XSMoShiStep1_1 = "十分顺利合成了 3",

	XSMoShiStep2_1 = "当您移动数字的时候\n新的数字将会出现",
	XSMoShiStep2_2 = "3 + 3 = 6",
	XSMoShiStep2_3 = "您成功的把 2 个 3 合成了 6",

	XSMoShiStep3_1 = "数字 3 和更大的数字\n只能和同样的数字合在一起\n尝试合出 12",
	XSMoShiStep3_2 = "棒极了!",

	XSMoShiStep4_1 = "1 只能和 2 合在一起",
	XSMoShiStep4_2 = "2 只能和 1 合在一起",
	XSMoShiStep4_3 = "合出一个 24 吧",
	XSMoShiStep4_4 = "这就是 THREES",
	XSMoShiStep4_5 = "尽量获取高分",

	BeginLayer_retry = "开始游戏",
	BeginLayer_back = "返回游戏",
	BeginLayer_Option = "游戏设置",
	BeginLayer_Music = "背景音乐",
	BeginLayer_Effects = "音效",
	BeginLayer_High = "最高分",
	BeginLayer_Most_1 = "数字",
	BeginLayer_Most_2 = "最大出现数",

	FKJiLu = "恭喜你获得了\n新的记录"
}