function FangKuaiManager:nextFangKuai()
	--根据上一个方块,在当前游戏内添加一个方块
	fangKuaiManager:nextFangKuaiIn()
	--随机生成下一个方块
	fangKuaiManager:nextFangKuaiRandom()
end

function FangKuaiManager:nextFangKuaiIn()
	local fangXiang = fangKuaiManager.moveFangXiang
	local kongTable = {}
	--根据方向把可能生成方块的位置,加入空集合
	if fangXiang == "shang" then
		for i=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[1][i]
			if fk.fenShu == 0 then
				table.insert(kongTable,fk)
			end
		end
	end

	if fangXiang == "xia" then
		for i=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[4][i]
			if fk.fenShu == 0 then
				table.insert(kongTable,fk)
			end
		end
	end

	if fangXiang == "left" then
		for i=1,HANG do
			local fk = fangKuaiManager.fangKuaiTable[i][4]
			if fk.fenShu == 0 then
				table.insert(kongTable,fk)
			end
		end
	end

	if fangXiang == "right" then
		for i=1,HANG do
			local fk = fangKuaiManager.fangKuaiTable[i][1]
			if fk.fenShu == 0 then
				table.insert(kongTable,fk)
			end
		end
	end

	--在需要生成星星的位置,随机生成一个星星
	local fk = kongTable[math.ceil(math.random()*table.getn(kongTable))]
	fk.fenShu = fangKuaiManager.nextFenShu
	fk:create()
end

--自动生成下一个方块
function FangKuaiManager:nextFangKuaiRandom()
	local lanNum = 0
	local redNum = 0
	for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			if fk.fenShu == 1 then
				lanNum = lanNum + 1
			end
			if fk.fenShu == 2 then
				redNum = redNum + 1
			end
		end
	end

	local random = math.random()
	if random < MAKELANGAILV then
		--下一个生成蓝色方块
		if lanNum  >= redNum + 4 then
			fangKuaiManager.nextFenShu = 2
			gameLayer.nextFKSp:setTexture(CCSprite:create("next-2.png"):getTexture())
		else
			fangKuaiManager.nextFenShu = 1
			gameLayer.nextFKSp:setTexture(CCSprite:create("next-1.png"):getTexture())
		end
	elseif random < MAKELANGAILV + MAKEHONGGAILV then
		--下一个生成红色方块
		if redNum  >= lanNum + 4 then
			fangKuaiManager.nextFenShu = 1
			gameLayer.nextFKSp:setTexture(CCSprite:create("next-1.png"):getTexture())
		else
			fangKuaiManager.nextFenShu = 2
			gameLayer.nextFKSp:setTexture(CCSprite:create("next-2.png"):getTexture())
		end
	elseif random < MAKEFEIBAIGAILV then
		--概率不足以生成白色方块,必定生成蓝色和红色较少的方块
		if lanNum < redNum then
			fangKuaiManager.nextFenShu = 1
			gameLayer.nextFKSp:setTexture(CCSprite:create("next-1.png"):getTexture())
		else
			fangKuaiManager.nextFenShu = 2
			gameLayer.nextFKSp:setTexture(CCSprite:create("next-2.png"):getTexture())
		end
	else
		--下一个生成白色方块
		--若最大数字1/8不到3则必为3
		if fangKuaiManager.topFen / 8 <= 3 then
			fangKuaiManager.nextFenShu = 3
		else
			--最大数字1/8大于3则可能生成高分方块
			local random2 = math.random()
			--根据概率生成白色分数
			if random2 < MAKEBAI3GAILV then
				fangKuaiManager.nextFenShu = 3
			else
				--剩余的其他数字平均分
				local random3 = math.random() 
				--计算出除了3之外,还有几种分数
				local num = math.log( fangKuaiManager.topFen/3,2) - 3
				fangKuaiManager.nextFenShu = 3*math.pow(2,math.ceil(random3*num))
			end
		end
		gameLayer.nextFKSp:setTexture(CCSprite:create("next-3.png"):getTexture())
	end
end