ShengYin=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                         for i,v in pairs(agr) do
                            obj[i] = v
                         end
                    end
                end )

function ShengYin:new()
	-- 判断是否播放背景音乐
	self.isPlayBackMusic = true;
	-- 判断是否播放音效
	self.isPlayEffect = true;
	-- 临时保存背景音乐
	self.backMusic = {file="",loop=true}
	-- 从本地文件里取设置
	local defaul = CCUserDefault:sharedUserDefault();
	self.isPlayBackMusic = (defaul:getIntegerForKey("isPlayBackMusic")~=1 or false);
	self.isPlayEffect = (defaul:getIntegerForKey("isPlayEffect")~=1 or false);

	shengYin = self
	self:init()
end

function ShengYin:init()
	
end

-- 设置是否播放音效
function settingPlayEffect()
    -- 判断是否开启声音
    shengYin.isPlayEffect = not shengYin.isPlayEffect;
    CCUserDefault:sharedUserDefault():setIntegerForKey("isPlayEffect",((shengYin.isPlayEffect and 0) or 1));
    CCUserDefault:sharedUserDefault():flush();
    return shengYin.isPlayEffect;
end

-- 设置是否播放背景音乐
function settingPlayBackMusic()
    -- 判断是否开启声音
    if shengYin.isPlayBackMusic then
        shengYin.isPlayBackMusic = false;
        SimpleAudioEngine:sharedEngine():stopBackgroundMusic();
    else
        shengYin.isPlayBackMusic = true;
        SimpleAudioEngine:sharedEngine():playBackgroundMusic(shengYin.backMusic.file, shengYin.backMusic.loop);
    end
    
    CCUserDefault:sharedUserDefault():setIntegerForKey("isPlayBackMusic",((shengYin.isPlayBackMusic and 0) or 1));
    CCUserDefault:sharedUserDefault():flush();
    return shengYin.isPlayBackMusic;
end

-- 播放背景音乐
function playBackMusic(file,loop)
    shengYin.backMusic.file = file;
	shengYin.backMusic.loop = loop;
    if  shengYin.isPlayBackMusic then
        SimpleAudioEngine:sharedEngine():playBackgroundMusic(file, loop);
    end
end

-- 播放音效
function playEffect(file)
    if  shengYin.isPlayEffect then
        SimpleAudioEngine:sharedEngine():playEffect(file);
    end
end