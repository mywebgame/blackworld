require "lua/games/MapConfig"
require "lua/games/MonsterConfig"
require "lua/games/SkillConfig"
require "lua/games/PropsConfig"


GameInfo = {
    mapConfig = nil,
    monsterConfig = nil,
    propConfig = nil,
    skillConfig = nil
}