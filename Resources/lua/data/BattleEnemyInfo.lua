require "lua/extern"

BattleEnemyInfo = {}
BattleEnemyInfo.__index = BattleEnemyInfo

function BattleEnemyInfo.create()
    --数据表
    local battleEnemyInfo = {}
    setmetatable(battleEnemyInfo,BattleEnemyInfo) 
    
    battleEnemyInfo.enemyBattleId = nil
    battleEnemyInfo.enemyChaId = nil
    battleEnemyInfo.enemyActionId = nil
    battleEnemyInfo.enemyChangeHp = nil
    battleEnemyInfo.enemyBuff = nil
    battleEnemyInfo.enemyCurrentHp = nil
    battleEnemyInfo.enemyTotalHp = nil
    
    return battleEnemyInfo
end