require "lua/extern"

BattleStepInfo = {}
BattleStepInfo.__index = BattleStepInfo

  

function BattleStepInfo.create()
    --数据表
    local battleStepInfo = {}
    setmetatable(battleStepInfo,BattleStepInfo) 
    
    battleStepInfo.chaBattleId = nil
    battleStepInfo.chaBuff = nil
    battleStepInfo.chaExpendHp = nil
    battleStepInfo.chaId = nil
    battleStepInfo.txtEffectId= nil
    battleStepInfo.actionId = nil
    battleStepInfo.chaCurrentHp = nil
    battleStepInfo.chaTotalHp = nil
    battleStepInfo.skill = nil
    battleStepInfo.enemyChaArr = {}

    return battleStepInfo
end