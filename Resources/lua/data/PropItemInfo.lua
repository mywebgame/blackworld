require "lua/extern"

PropItemInfo = {}
PropItemInfo.__index = PropItemInfo

function PropItemInfo.create()
    local propItem = {}          
    setmetatable(propItem,PropItemInfo) 
    
    propItem.bodyType = nil
    propItem.name = nil
    propItem.baseQuality = nil
    propItem.description = nil
    propItem.id = nil
    propItem.icon = nil
    propItem.maxexp = nil
    propItem.growTemp = nil
    propItem.comprice = nil
    propItem.compound = nil

    return propItem
end