require "AudioEngine" 
require "lua/utils/utils"
require "lua/Layers/Layer_Loading"
require "lua/data/UserInfo"
require "lua/data/GameInfo"
require "lua/network/NetInfo"
require "lua/games/MapConfig"

-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)
    print("----------------------------------------")
    print("LUA ERROR: " .. tostring(msg) .. "\n")
    print(debug.traceback())
    print("----------------------------------------")
end

local mapConfig = nil

--------- SocketManager set
function setSocketManager(sManager)
    NetInfo.socketManager = sManager
end

local function main()
    -- avoid memory leak
    collectgarbage("setpause", 100)
    collectgarbage("setstepmul", 5000)
end

local function battleTest(nodeScene)

    local mfContent = CCString:createWithContentsOfFile(CCFileUtils:sharedFileUtils():fullPathForFilename("games/battle_test.json")):getCString()
    local jsonData = cjson.decode(mfContent)

    require "lua/Layers/Layer_Battle"
    nodeScene:addChild(battleLayer(jsonData))
end

function doInit()
    
    --加载地图怪物信息
    --地图信息会依赖怪物信息，要先初始化怪物信息
    GameInfo.monsterConfig = MonsterConfig.create()
    GameInfo.monsterConfig:loadData()

    --加载副本地图信息
    GameInfo.mapConfig = MapConfig.create()
    GameInfo.mapConfig:loadData()
    
    --加载道具信息
    GameInfo.propConfig = PropsConfig.create()
    GameInfo.propConfig:loadData()

    --加载技能信息
    GameInfo.skillConfig = SkillConfig.create()
    GameInfo.skillConfig:loadData()

    ---------------
    local sceneGame = CCScene:create()
    CCDirector:sharedDirector():runWithScene(sceneGame)
    
    --login 界面加载
    require "lua/Layers/Layer_Login"
    sceneGame:addChild(loginLayer())
--    battleTest(sceneGame)

    local bIsOk = NetInfo.socketManager:connectServer()    
    if bIsOk then
        --CCMessageBox("成功连接服务器","")
    else
        CCMessageBox("连接服务器失败","")
    end

end

xpcall(main, __G__TRACKBACK__)












