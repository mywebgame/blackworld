
cclog = function(...)
        print(string.format(...))
end

utils = {index = 1}   

function utils.getMsgsFromQuene(quene,msgId)
    local msgRet = CCArray:create()
    local msgArray = quene:getArray()
    local msgCount = msgArray:count()
    
    for i = 0,msgCount-1,1 do
        if msgCount ~= 0 then
            local msgTest = tolua.cast(msgArray:objectAtIndex(i),"IOMessage")
            if msgTest:getCommondId() == msgId then
                msgRet:addObject(msgTest)
            end
        end
    end
    msgArray:removeObjectsInArray(msgRet)
    return msgRet
end

function utils.getAnimationFromFile(fileName)
    local animationFilePath = CCFileUtils:sharedFileUtils():fullPathForFilename(fileName)
    
    local dicAnimation= CCDictionary:createWithContentsOfFile(animationFilePath)
    
    local delayPerUnit = dicAnimation:valueForKey("delayPerUnit"):floatValue()
    local arrayFrameNames = tolua.cast(dicAnimation:objectForKey("frames"),"CCArray")
    local framesCount = arrayFrameNames:count()

    local arraySpriteFrames = CCArray:create()
    if framesCount > 0 then
        for i=0,framesCount-1,1 do
            local frameName = tolua.cast(arrayFrameNames:objectAtIndex(i),"CCString")
            local spriteFrame = CCSpriteFrameCache:sharedSpriteFrameCache():spriteFrameByName(frameName:getCString())
            arraySpriteFrames:addObject(spriteFrame)
        end
    end

    local animation = CCAnimation:createWithSpriteFrames(arraySpriteFrames)
    animation:setDelayPerUnit(delayPerUnit)
    return animation
end

