require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/Layers/Layer_EnterName"
require "lua/Layers/Layer_HomePage"
require "lua/network/NetInfo"

local scheduler = CCDirector:sharedDirector():getScheduler()

LayerSelectRole = LayerSelectRole or {}
ccb["LayerSelectRole"] = LayerSelectRole

local socketManager

local layer = nil
local enLayer = nil
local roleIndex = 1
local schedule_handleMessages_id = nil

 
local function handleMsg102()
    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),102)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
            
        if bResult then
            --移除输入角色名对话框
            enLayer:removeFromParentAndCleanup(true)

            --成功创建了角色
            local userValues = jsonData.data

            UserInfo.characterId = userValues.newCharacterId
            UserInfo.userId = userValues.userId
            
            local tabRoleInfo ={}
            tabRoleInfo["userId"] = UserInfo.userId
            tabRoleInfo["characterId"] = UserInfo.characterId

            --构造json
            --发送获取玩家信息消息
            local jsonData = cjson.encode(tabRoleInfo)
            NetInfo.socketManager:sendMessage(jsonData,103)
        else
            cclog("handleMsg102 error")
        end

    end
end

local function handleMsg103()
    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),103)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        LoadingLayer.dismiss()
        
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
        local data = jsonData.data
        
        if bResult then
            --处理新角色信息
            local uName = data.name
            local uPower = data.power
            local uCid = data.cid
            local uGas = data.gas
            local uLevel = data.level
            local uProfession = data.profession
            local uMaxExp = data.maxexp
            local uExp = data.exp
            local uCoin = data.coin
            local uYuanbao = data.yuanbao
            
            UserInfo.name = uName
            UserInfo.power = uPower
            UserInfo.characterId = uCid
            UserInfo.gas = uGas
            UserInfo.level = uLevel
            UserInfo.profession = uProfession
            UserInfo.maxexp = uMaxExp
            UserInfo.exp = uExp
            UserInfo.coin = uCoin
            UserInfo.yuanbao = uYuanbao
            
            --进入home界面
            local hpLayer = homePageLayer()
            local hpScene = CCScene:create()
            hpScene:addChild(hpLayer)
            CCDirector:sharedDirector():replaceScene(hpScene)
        else
            cclog("handleMsg103 error")
        end

    end
end


--处理消息
local function handleMessages()
    if NetInfo.socketManager ~= nil then
        handleMsg102()
        handleMsg103()
    end
end


local function onEnter()
    schedule_handleMessages_id = scheduler:scheduleScriptFunc(handleMessages, 0.1, false)
end

local function onExit()
    scheduler:unscheduleScriptEntry(schedule_handleMessages_id)
end

function selectRoleLayer()

    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_selectrole.ccbi",proxy,LayerSelectRole)
    layer = tolua.cast(node,"CCLayer")
    
    --注册事件
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end

    layer:registerScriptHandler(onNodeEvent)
    return layer
end

--选人事件触发
local function onCCControlButtonSelectRole1Clicked()
    roleIndex=1
    
    local spriteOuter = tolua.cast(LayerSelectRole["spriteOuter"],"CCSprite")
    local nodeRole1 = tolua.cast(LayerSelectRole["roleNode1"],"CCNode")
    local labelHeroDescrip = tolua.cast(LayerSelectRole["labelRoleDescrip"],"CCLabelTTF")
    local spriteHero = tolua.cast(LayerSelectRole["spriteRole"],"CCSprite")

    local x,y = nodeRole1:getPosition()
    spriteOuter:setPosition(ccp(x,y))
    labelHeroDescrip:setString("齐天大圣：武僧，拥有高生命高防御")
    spriteHero:initWithFile("UI/qs_0001.png")
end

local function onCCControlButtonSelectRole2Clicked()
    roleIndex=2

    local spriteOuter = tolua.cast(LayerSelectRole["spriteOuter"],"CCSprite")
    local nodeRole2 = tolua.cast(LayerSelectRole["roleNode2"],"CCNode")
    local labelHeroDescrip = tolua.cast(LayerSelectRole["labelRoleDescrip"],"CCLabelTTF")
    local spriteHero = tolua.cast(LayerSelectRole["spriteRole"],"CCSprite")

    local x,y = nodeRole2:getPosition()
    spriteOuter:setPosition(ccp(x,y))
    labelHeroDescrip:setString("蛮族之王：狂战士，拥有高攻击")
    spriteHero:initWithFile("UI/qs_0002.png")
end

local function onCCControlButtonSelectRole3Clicked()
    roleIndex=3

    local spriteOuter = tolua.cast(LayerSelectRole["spriteOuter"],"CCSprite")
    local nodeRole3 = tolua.cast(LayerSelectRole["roleNode3"],"CCNode")
    local labelHeroDescrip = tolua.cast(LayerSelectRole["labelRoleDescrip"],"CCLabelTTF")
    local spriteHero = tolua.cast(LayerSelectRole["spriteRole"],"CCSprite")

    local x,y = nodeRole3:getPosition()
    spriteOuter:setPosition(ccp(x,y))
    labelHeroDescrip:setString("恶魔统帅：术士，拥有高速度，可先手攻击")
    spriteHero:initWithFile("UI/qs_0003.png")
end

--选人结束了
local function onCCControlButtonSelectRoleOkClicked()
    enLayer = enterNameLayer(roleIndex)
    layer:addChild(enLayer)
end

LayerSelectRole["onCCControlButtonSelectRole1Clicked"] = onCCControlButtonSelectRole1Clicked
LayerSelectRole["onCCControlButtonSelectRole2Clicked"] = onCCControlButtonSelectRole2Clicked
LayerSelectRole["onCCControlButtonSelectRole3Clicked"] = onCCControlButtonSelectRole3Clicked
LayerSelectRole["onCCControlButtonSelectRoleOkClicked"] = onCCControlButtonSelectRoleOkClicked












