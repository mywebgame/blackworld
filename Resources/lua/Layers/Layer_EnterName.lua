require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/network/NetInfo"

LayerEnterName = LayerEnterName or {}
ccb["LayerEnterName"] = LayerEnterName

local layer = nil
local editName = nil
local selectIndex = nil

local function onEnter()

end

local function onExit()
    layer:unregisterScriptTouchHandler()
end

local function onTouch(event, x, y)
   
    local enterNamePanel = tolua.cast(LayerEnterName["enterNamePanel"],"CCSprite") 
    if enterNamePanel:boundingBox():containsPoint(ccp(x,y)) then
        return false
    end

    return true
end

function enterNameLayer(roleIndex)
    selectIndex = roleIndex
    
    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_entername.ccbi",proxy,LayerEnterName)
    layer = tolua.cast(node,"CCLayer")
    
    local txtBack = tolua.cast(LayerEnterName["textFieldBack"],"CCSprite")    
    
    local nX,nY = txtBack:getPosition()
    local backSize = txtBack:getContentSize()

    editName = CCEditBox:create(CCSizeMake(backSize.width,backSize.height), CCScale9Sprite:create())
    editName:setPosition(ccp(nX,nY))
    
    local enterNamePanel = tolua.cast(LayerEnterName["enterNamePanel"],"CCSprite")    
    if nil ~= enterNamePanel then
        enterNamePanel:addChild(editName)
    end
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end

    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, 0, true);
    
    return layer
end

local function onCCControlButtonEnterNameClicked()
    
    local roleName = editName:getText()
    
    if string.len(roleName) < 5 then
        CCMessageBox("user name is too short","")
        return
    end
    
    --弹出Loading界面
    LoadingLayer.show()

    -------角色信息-------
    local tabRoleInfo ={}
    tabRoleInfo["rolename"] = roleName
    tabRoleInfo["profession"] = selectIndex
    
    --构造json
    local jsonData = cjson.encode(tabRoleInfo)
    NetInfo.socketManager:sendMessage(jsonData,102)
end

LayerEnterName["onCCControlButtonEnterNameClicked"] = onCCControlButtonEnterNameClicked










