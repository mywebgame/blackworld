require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/Layers/Layer_ConfirmEmploy"
require "lua/network/NetInfo"
require "lua/objects/Object_FriendCell"

LayerEmployFriends = LayerEmployFriends or {}
ccb["LayerEmployFriends"] = LayerEmployFriends

local layer = nil
local confirmEmployLayer = nil
local schedule_handleMessages_id = nil
local friend_list = {}
local scheduler = CCDirector:sharedDirector():getScheduler()
local tagFriendsTable = 9998
local friend_selected = nil

--获取更多好友
local function requestFriends(itemIndex)

    local msgEmployInfo ={}
    msgEmployInfo["characterId"] = UserInfo.characterId
    msgEmployInfo["index"] = itemIndex
    msgEmployInfo["tag"] = 2

    local jsonEmployInfo = cjson.encode(msgEmployInfo)
    NetInfo.socketManager:sendMessage(jsonEmployInfo,302)
end

--table delegate
function tableCellTouched(tablex,cell)
    if cell:getIdx() == table.getn(friend_list) then
        requestFriends(table.getn(friend_list))
    else
        --弹出雇佣界面
        local itemIndex = cell:getIdx()
        local friendCell = friend_list[itemIndex+1]
        if friendCell.bEmploy then
            CCMessageBox("此好友已经被雇佣","")
            return
        end

        friend_selected = friendCell
        confirmEmployLayer = comfirmEmployLayer(friendCell.chaid)
        layer:addChild(confirmEmployLayer)
    end
end

function cellSizeForTable(tablex,idx) 
    return 91,91
end

function tableCellAtIndex(tablex, idx)

--    local cell = tablex:dequeueCell()
    local cell = nil
    local label = nil
    if nil == cell then
        
        if idx == table.getn(friend_list) then
           cell = CCTableViewCell:new()
           local label = CCLabelTTF:create("点击加载数据...", "Helvetica", 20.0)
           label:setPosition(CCPointMake(200,40))
           cell:addChild(label) 
        else
            local fCellData = friend_list[idx+1]
            local friendCell = Object_FriendCell.create()
            cell = CCTableViewCell:new()
            friendCell.Node:setPosition(ccp(200,40))
            friendCell:setStringLevel(fCellData.level)
            if fCellData.bEmploy then
                friendCell:setStringName(fCellData.rolename .. "  已经雇佣")
            else
                friendCell:setStringName(fCellData.rolename)
            end
            
            friendCell:setStringSkill(fCellData.skill)
            friendCell:setStringMoney(fCellData.price)
            cell:addChild(friendCell.Node)
        end
    else
        local friendCell = friend_list[idx+1]
        local nodeUI = cell:getChildByTag(2014)
        local nodeBack = nodeUI:getChildByTag(999)
        local nodeLevel = nodeBack:getChildByTag(999)
        local nodeName = nodeBack:getChildByTag(1000)
        local nodeSkill = nodeBack:getChildByTag(1001)
        local nodePrice = nodeBack:getChildByTag(1002)
        
        local labelName = tolua.cast(nodeName,"CCLabelTTF")
        local labelLevel = tolua.cast(nodeLevel,"CCLabelTTF")
        local labelSkill = tolua.cast(nodeSkill,"CCLabelTTF")
        local labelPrice = tolua.cast(nodePrice,"CCLabelTTF")
        
        labelName:setString(friendCell.rolename)
        labelLevel:setString(friendCell.level)
        labelSkill:setString(friendCell.skill)
        labelPrice:setString(friendCell.price)

    end

    return cell
end

function numberOfCellsInTableView(tablex)
   return table.getn(friend_list)+1
end


--（2301消息）处理获取好友雇佣请求
local function handleMsg2301()
    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),2301)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
        local data = jsonData.data
        
        if bResult then
            cclog("recv msg2301 ok , parsing...")
            friend_selected.bEmploy = true
            if confirmEmployLayer ~= nil then
                confirmEmployLayer:removeFromParentAndCleanup(true)
            end
            local scrollFriendList = tolua.cast(LayerEmployFriends["scrollFriendList"],"CCScrollView")
            local tableFriends = scrollFriendList:getParent():getChildByTag(tagFriendsTable)
            tableFriends:reloadData()
        else
            cclog("handleMsg2301 error")
            if confirmEmployLayer ~= nil then
                confirmEmployLayer:removeFromParentAndCleanup(true)
            end
        end
        confirmEmployLayer = nil 
        LoadingLayer.dismiss()

        CCMessageBox(message,"")
    end
end

--（302消息）处理获取好友列表消息
local function handleMsg302()

    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),302)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
        local data = jsonData.data

        if bResult then
            cclog("recv msg302 ok , parsing...")

            local originNum = table.getn(friend_list)
            local listNum = table.getn(data.friendlist)
            for i=1, listNum,1 do
                local friendData = data.friendlist[i]
                
                local friendCell = Object_FriendCell.create()
                friendCell.level = friendData.level
                friendCell.price = friendData.price
                friendCell.bEmploy = friendData.guyong
                friendCell.chaid = friendData.chaid
                friendCell.rank = friendData.rank
                friendCell.rolename = friendData.rolename
                friendCell.skill = friendData.skill

                --存储信息
                friend_list[originNum+i] = friendCell
            end
            
            --加载cell到页面
            local currentNum = table.getn(friend_list)
            local scrollFriendList = tolua.cast(LayerEmployFriends["scrollFriendList"],"CCScrollView")
            local viewSize = scrollFriendList:getViewSize()
            local vX,vY = scrollFriendList:getPosition()
            
            local tableFriends = scrollFriendList:getParent():getChildByTag(tagFriendsTable)
            if tableFriends == nil then
                tableFriends = CCTableView:create(viewSize)
                tableFriends:setTag(tagFriendsTable)
                tableFriends:setPosition(ccp(vX,vY))
                tableFriends:setVerticalFillOrder(kCCTableViewFillTopDown);
                scrollFriendList:getParent():addChild(tableFriends)
                
                tableFriends:registerScriptHandler(tableCellTouched,CCTableView.kTableCellTouched)
                tableFriends:registerScriptHandler(cellSizeForTable,CCTableView.kTableCellSizeForIndex)
                tableFriends:registerScriptHandler(tableCellAtIndex,CCTableView.kTableCellSizeAtIndex)
                tableFriends:registerScriptHandler(numberOfCellsInTableView,CCTableView.kNumberOfCellsInTableView)  
                
            end
            tableFriends:reloadData()
            
        else
            cclog("handleMsg4500 error")
        end
        LoadingLayer.dismiss()
    end


end

--处理消息
local function handleMessages()
    if NetInfo.socketManager ~= nil then
        handleMsg302()
        handleMsg2301()
    end
end

--


local function onEnter()
    schedule_handleMessages_id = scheduler:scheduleScriptFunc(handleMessages, 1, false)
    friend_list = {}
    --请求索引项为0开始的好友
    requestFriends(0)
end

local function onExit()
    layer:unregisterScriptTouchHandler()
    scheduler:unscheduleScriptEntry(schedule_handleMessages_id)
end

local function onTouch(event, x, y)
    return true
end

function employFriendsLayer()

    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_employfriends.ccbi",proxy,LayerEmployMain)
    layer = tolua.cast(node,"CCLayer")
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end
    layer:registerScriptHandler(onNodeEvent)


    local btnClose = tolua.cast(LayerEmployFriends["btnClose"],"CCControlButton")
    
    btnClose:setTouchPriority(-128)

    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, 0, true)
    
    return layer
end

local function clickAtCloseButton()
    --移除界面
    layer:removeFromParentAndCleanup(true)
end

LayerEmployFriends["clickAtCloseButton"] = clickAtCloseButton






