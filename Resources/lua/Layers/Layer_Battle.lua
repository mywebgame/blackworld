require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/network/NetInfo"
require "lua/objects/CardCharactor"
require "lua/data/BattleStepInfo"
require "lua/data/BattleEnemyInfo"

LayerBattle = LayerBattle or {}
ccb["LayerBattle"] = LayerBattle

local layer = nil
local battleData = nil
local battleSteps = {}
local battleCharactors = {}
local actionTestTime = 2.5

--当前回合
local battleRound = 1
--每个回合的战斗时间 
local battleRoundIdleTime = 1.0

local function onEnter()
    
end

local function onExit()
    layer:unregisterScriptTouchHandler()
end

local function onTouch(event, x, y)
    return true
end

--根据id获取相应对象
local function getCharactorById(cId)
    local charactorRet = nil
    local charactorNum = table.getn(battleCharactors)

    if charactorNum > 0  then
        for i = 1 , charactorNum, 1 do
            local charactorTest = battleCharactors[i]
            if charactorTest.chaId == cId then
                charactorRet = charactorTest
                break
            end
        end
    end
    
    return charactorRet
end

-- 播放技能动画
local function hurtEffect(enemyInfo)
    local cCharactor = getCharactorById(enemyInfo.enemyChaId)
    if cCharactor == nil then
        return
    end
    
    local spriteHurt = CCSprite:create("UI/attacked_img.png");
    spriteHurt:setOpacity(0)
    spriteHurt:setPosition(ccp(0, 0));
    cCharactor:getUI():addChild(spriteHurt)
    
    --
    local arrActions = CCArray:create()
    arrActions:addObject(CCDelayTime:create(0.2))
    arrActions:addObject(CCFadeTo:create(0.1,255))
    arrActions:addObject(CCDelayTime:create(0.4))
    arrActions:addObject(CCRemoveSelf:create())
    
    spriteHurt:runAction(CCSequence:create(arrActions))
end

--对敌人发起攻击,敌人的数值发生变化,及受伤动画
local function attackEnemy(enemyInfo)
    local cCharactor = getCharactorById(enemyInfo.enemyChaId)
    if cCharactor == nil then
        cclog("enemy not found--> "..enemyInfo.enemyChaId)
        return
    end
    
    --飙血动画
    local resultHP = enemyInfo.enemyCurrentHp + enemyInfo.enemyChangeHp
    if resultHP < 0 then
        resultHP = 0
    end
    local currentPercentage = resultHP / enemyInfo.enemyTotalHp * 100
    cCharactor:getBlood():runAction(CCProgressFromTo:create(0.2, cCharactor:getBlood():getPercentage(), currentPercentage))
    cCharactor.chaCurrentHp = resultHP
    

    --振动动画
    local cX,cY = cCharactor:getUI():getPosition()
    local moveLeft=CCMoveTo:create(0.03, ccp(cX+5,cY));
    local moveRight=CCMoveTo:create(0.03, ccp(cX-5, cY));
    local moveTop=CCMoveTo:create(0.03, ccp(cX, cY+5));
    local moveOrigin=CCMoveTo:create(0.03, ccp(cX, cY));
    
    local arrActions = CCArray:create()
    arrActions:addObject(CCDelayTime:create(0.2))
    arrActions:addObject(moveLeft)
    arrActions:addObject(moveRight)
    arrActions:addObject(moveTop)
    arrActions:addObject(moveOrigin)
 
    if resultHP == 0 then
        arrActions:addObject(CCDelayTime:create(0.2))
        arrActions:addObject(CCRemoveSelf:create())
    end

    --播放受伤动作
    cCharactor:getUI():runAction(CCSequence:create(arrActions))
    --播放受伤特效
    hurtEffect(enemyInfo)
end

-- 播放技能动画
local function skillEffect(stepInfo)
   
    local battle_back = tolua.cast(LayerBattle["battle_back"],"CCSprite")
    
    local spriteAttack = CCSprite:create("UI/attack_img.png");
    battle_back:addChild(spriteAttack)
    
    local cCharactor = getCharactorById(stepInfo.chaId)
    local cX,cY = cCharactor:getUI():getPosition()

    spriteAttack:setPosition(ccp(cX, cY+30))
    if math.floor(stepInfo.chaBattleId/10) == 2 then
        spriteAttack:setRotation(180);
        spriteAttack:setPosition(ccp(cX, cY-30))
    end
    
    local moveon = nil
    if math.floor(stepInfo.chaBattleId/10) == 2 then
         moveon = CCMoveTo:create(0.4/actionTestTime, ccp(cX, cY-30))
    elseif math.floor(stepInfo.chaBattleId/10) == 1 then
         moveon = CCMoveTo:create(0.4/actionTestTime, ccp(cX, cY+30))
    end

    
    local removeSelf = CCRemoveSelf:create()

    local arrActions = CCArray:create()
    arrActions:addObject(moveon)
    arrActions:addObject(removeSelf)

    --执行动画
    spriteAttack:runAction(CCSequence:create(arrActions))

end

--进行攻击
local function attackAction(stepInfo)
    local cCharactor = getCharactorById(stepInfo.chaId)
    
    --未找到
    if cCharactor == nil then
        return
    end

    local cX,cY = cCharactor:getUI():getPosition()
    local moveon=CCMoveTo:create(0.12/actionTestTime, ccp(cX, cY+20));
    local movedown=CCMoveTo:create(0.12/actionTestTime, ccp(cX, cY));
    
    local arrActions = CCArray:create()
    arrActions:addObject(moveon)
    arrActions:addObject(movedown)
    
    --执行动画
    cCharactor:getUI():runAction(CCSequence:create(arrActions))
    
    --播放技能动画
    skillEffect(stepInfo)
end

--战斗结束，显示战斗结果界面
local function battleFinished()
    local battleResultData = battleData.data.setData
    local battleResult = battleData.data.battleResult
    
    if battleResult == 1 then 
        require "lua/Layers/Layer_BattleWin"
        layer:addChild(battleWinLayer(battleResultData))
    else
        require "lua/Layers/Layer_BattleFailed"
        layer:addChild(battleFailedLayer(battleResultData))
    end
    
end

local function goBattle(sender)
    cclog("开始战斗")
    
    --根据step信息，播放战斗过程
    local stepNum = table.getn(battleSteps)
    if battleRound <= stepNum then
        local stepInfo = battleSteps[battleRound]
        local enemyNum = table.getn(stepInfo.enemyChaArr)
        
        --对敌人发起攻击
        if enemyNum > 0 then
            for j=1,enemyNum,1 do
                local enemyInfo = stepInfo.enemyChaArr[j]
                attackEnemy(enemyInfo)
                cclog(stepInfo.chaId .. " 对 " .. enemyInfo.enemyChaId .. " 发起攻击")
            end
            --播放攻击过程及受伤飙血动画
            attackAction(stepInfo)
        end
        battleRound = battleRound+1
    end
    
    if battleRound <= stepNum then
        local arrActions = CCArray:create()
        arrActions:addObject(CCDelayTime:create(battleRoundIdleTime))
        arrActions:addObject(CCCallFunc:create(goBattle))
        --执行动画
        layer:runAction(CCSequence:create(arrActions))
    else
        --战斗结束了@v@
        local arrActions = CCArray:create()
        arrActions:addObject(CCDelayTime:create(battleRoundIdleTime))
        arrActions:addObject(CCCallFunc:create(battleFinished))
        layer:runAction(CCSequence:create(arrActions))
    end
end

--准备战斗 --（播放战斗前的一些动画等等）
local function prepareBattle()
    cclog("准备战斗")

    --播放动画 
    local animationBattleBegin = utils.getAnimationFromFile("animations/animation_battleprepare.plist")
    local spriteBegin = CCSprite:createWithSpriteFrameName("01.png")
    
    local arrActions = CCArray:create()
    arrActions:addObject(CCDelayTime:create(0.4))
    arrActions:addObject(CCAnimate:create(animationBattleBegin))
    arrActions:addObject(CCDelayTime:create(0.6))
    arrActions:addObject(CCRemoveSelf:create())
    arrActions:addObject(CCCallFuncN:create(goBattle))
    --执行动画
    spriteBegin:runAction(CCSequence:create(arrActions))
    spriteBegin:setPosition(ccp(320,480))

    layer:addChild(spriteBegin)
end

--解析steps
local function loadSteps(stepsData)
    local stepNum = table.getn(stepsData)
    --解析
    if stepNum > 0 then
        for i=1, stepNum, 1 do
            local stepData = stepsData[i]
            
            local battleStepInfo = BattleStepInfo.create()
            battleStepInfo.chaBattleId = stepData.chaBattleId
            battleStepInfo.chaBuff = stepData.chaBuff
            battleStepInfo.chaExpendHp = stepData.chaExpendHp
            battleStepInfo.chaId = stepData.chaId
            battleStepInfo.txtEffectId = stepData.txtEffectId
            battleStepInfo.actionId = stepData.actionId
            battleStepInfo.chaCurrentHp = stepData.chaCurrentHp
            battleStepInfo.chaTotalHp = stepData.chaTotalHp
            battleStepInfo.skill = stepData.skill
            
            local enemyNum = table.getn(stepData.enemyChaArr)
            for j=1, enemyNum, 1 do
                local enemyInfo = stepData.enemyChaArr[j]
                local battleEnemy = BattleEnemyInfo.create()
                
                battleEnemy.enemyBattleId = enemyInfo.enemyBattleId
                battleEnemy.enemyChaId = enemyInfo.enemyChaId
                battleEnemy.enemyActionId = enemyInfo.enemyActionId
                battleEnemy.enemyChangeHp = enemyInfo.enemyChangeHp
                battleEnemy.enemyBuff = enemyInfo.enemyBuff
                battleEnemy.enemyCurrentHp = enemyInfo.enemyCurrentHp
                battleEnemy.enemyTotalHp = enemyInfo.enemyTotalHp
                
                battleStepInfo.enemyChaArr[j] = battleEnemy
            end

            battleSteps[i] = battleStepInfo
        end
    end
end

--解析加载卡牌
local function loadCards(startData)
    local cardNum = table.getn(startData)
    
    --解析
    if cardNum > 0 then
        for i=1, cardNum,1 do
            local cardData = startData[i]
            local iconFile = string.format("UI/qs_%04d.png",cardData.chaIcon)
            
            local cCharactor = CardCharactor.create(iconFile)
            cCharactor:setChaBattleId(cardData.chaBattleId)
            cCharactor:setChaName(cardData.chaName)
            cCharactor:setChaPos(cardData.chaPos)
            cCharactor:setChaLevel(cardData.chaLevel)
            cCharactor:setChaDirection(cardData.chaDirection)
            cCharactor:setChaIcon(cardData.chaIcon)
            cCharactor:setChaCurrentHp(cardData.chaCurrentHp)
            cCharactor:setChaTotalHp(cardData.chaTotalHp)
            cCharactor:setChaId(cardData.chaId)

            --参与战斗的所有卡牌
            battleCharactors[i] = cCharactor
        end
    end
    
    --加载
    local battle_back = tolua.cast(LayerBattle["battle_back"],"CCSprite") 
    local battle_back_size = battle_back:getContentSize()
    if cardNum > 0 then
        for i=1, cardNum,1 do
            local cCharactor = battleCharactors[i]
            battle_back:addChild(cCharactor:getUI())
            cCharactor:getUI():setScale(0.6)
            
            local col = (cCharactor:getChaPos()-1) % 3
            local row = math.floor( (cCharactor:getChaPos()-1) / 3 )
            cclog("card----> ".."col = "..col..",row="..row)
            if cCharactor:getChaDirection() == 1 then
                cCharactor:getUI():setPosition(ccp(120 + col*120 ,100 + row*110))
            else
                cCharactor:getUI():setPosition(ccp(120 + col*120 ,480 + row*110))
            end
        end
    end
end


local function initBattle()
    --加入缓存
    local cache = CCSpriteFrameCache:sharedSpriteFrameCache()
    cache:addSpriteFramesWithFile("UI/jn_name.plist")
    cache:addSpriteFramesWithFile("UI/begin1.plist")
    cache:addSpriteFramesWithFile("UI/result_attacked.plist")
    
    --解析step data
    local stepsData = battleData.data.stepData
    loadSteps(stepsData)

    --解析start data
    local startData = battleData.data.startData
    loadCards(startData)
    
    --准备战斗
    battleRound = 1
    prepareBattle()
end

function battleLayer(jsonData)

    battleData = jsonData

    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_battle.ccbi",proxy,LayerBattleInfo)
    layer = tolua.cast(node,"CCLayer")
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end
    
    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, 0, true)
    
    --初始化战斗 
    initBattle()
    
    return layer
end








