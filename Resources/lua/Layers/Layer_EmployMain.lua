require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/network/NetInfo"

LayerEmployMain = LayerEmployMain or {}
ccb["LayerEmployMain"] = LayerEmployMain

local layer = nil

local function onEnter()

end

local function onExit()
    layer:unregisterScriptTouchHandler()
end

local function onTouch(event, x, y)
    return true
end

function employMainLayer()

    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_employmain.ccbi",proxy,LayerEmployMain)
    layer = tolua.cast(node,"CCLayer")
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end

    local btnClose = tolua.cast(LayerEmployMain["btnClose"],"CCControlButton")
    local btnEmployFriends = tolua.cast(LayerEmployMain["btnEmployFriends"],"CCControlButton")
    local btnEmployRecords = tolua.cast(LayerEmployMain["btnEmployRecords"],"CCControlButton")
    
    btnClose:setTouchPriority(-128)
    btnEmployFriends:setTouchPriority(-128)
    btnEmployRecords:setTouchPriority(-128)

    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, 0, true)
    
    return layer
end

local function clickAtEmployFriendsButton()
    --添加雇佣好友层
    require "lua/Layers/Layer_EmployFriends"
    layer:getParent():addChild(employFriendsLayer())

    --移除当前层
    layer:removeFromParentAndCleanup(true)
end

local function clickAtEmployListButton()
    -- 布阵界面
    layer:removeFromParentAndCleanup(true)
end

local function clickAtCloseButton()
    --移除界面
    layer:removeFromParentAndCleanup(true)
end

LayerEmployMain["clickAtEmployListButton"] = clickAtEmployListButton
LayerEmployMain["clickAtEmployFriendsButton"] = clickAtEmployFriendsButton
LayerEmployMain["clickAtCloseButton"] = clickAtCloseButton






