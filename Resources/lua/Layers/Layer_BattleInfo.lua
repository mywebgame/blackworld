require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/network/NetInfo"

LayerBattleInfo = LayerBattleInfo or {}
ccb["LayerBattleInfo"] = LayerBattleInfo

local layer = nil
local miInfo = nil

local function onEnter()

end

local function onExit()
    layer:unregisterScriptTouchHandler()
end

local function onTouch(event, x, y)
    return true
end

function battleInfoLayer(mapItemInfo)

    miInfo = mapItemInfo

    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_battleinfo.ccbi",proxy,LayerBattleInfo)
    layer = tolua.cast(node,"CCLayer")
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end
    
    local labelWarName = tolua.cast(LayerBattleInfo["labelWarName"],"CCLabelTTF")
    local labelWarDesc = tolua.cast(LayerBattleInfo["labelWarDesc"],"CCLabelTTF")
    local labelWarEnemy = tolua.cast(LayerBattleInfo["labelEnemyDesc"],"CCLabelTTF")
    
    local labelCoinGet = tolua.cast(LayerBattleInfo["labelCoinGet"],"CCLabelTTF")
    local labelExpGet = tolua.cast(LayerBattleInfo["labelExpGet"],"CCLabelTTF")
    local labelGoodGet = tolua.cast(LayerBattleInfo["labelGoodGet"],"CCLabelTTF")

    labelWarName:setString(mapItemInfo.nickname)
    labelWarDesc:setString(mapItemInfo.desc)
    labelWarEnemy:setString(mapItemInfo.enemydesc)
    labelCoinGet:setString("钱+"..mapItemInfo.coin)
    labelExpGet:setString("经验+"..mapItemInfo.exp)
    
    if mapItemInfo.exp == nil then
        labelGoodGet:setString("无掉落物")
    else
        labelGoodGet:setString("无掉落物")
    end
    
    local btnBattle = tolua.cast(LayerBattleInfo["btnBattle"],"CCControlButton")
    local btnLineUp = tolua.cast(LayerBattleInfo["btnLineUp"],"CCControlButton")
    local btnClose = tolua.cast(LayerBattleInfo["btnClose"],"CCControlButton")
    
    btnBattle:setTouchPriority(-128)
    btnLineUp:setTouchPriority(-128)
    btnClose:setTouchPriority(-128)

    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, 0, true)
    
    return layer
end

local function clickAtBattleButton()
cclog("userId="..UserInfo.userId)

    -- 进入 战斗 场景 
    local tabBattleInfo ={}
    tabBattleInfo["characterId"] = UserInfo.characterId
    tabBattleInfo["zjid"] = miInfo.bid

    --构造json ，发送开战消息
    local jsonData = cjson.encode(tabBattleInfo)
    NetInfo.socketManager:sendMessage(jsonData,4501)
    layer:removeFromParentAndCleanup(true)
end

local function clickAtLineUpButton()
    -- 布阵界面
    layer:removeFromParentAndCleanup(true)
end

local function clickAtCloseButton()
    --移除界面
    layer:removeFromParentAndCleanup(true)
end

LayerBattleInfo["clickAtBattleButton"] = clickAtBattleButton
LayerBattleInfo["clickAtLineUpButton"] = clickAtLineUpButton
LayerBattleInfo["clickAtCloseButton"] = clickAtCloseButton






