require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/Layers/Layer_BattleInfo"
require "lua/Layers/Layer_Battle"
require "lua/network/NetInfo"
require "lua/data/GameInfo"
require "lua/games/MapConfig"
require "lua/objects/MapItem"

local scheduler = CCDirector:sharedDirector():getScheduler()

LayerHomePage = LayerHomePage or {}
ccb["LayerHomePage"] = LayerHomePage

local layer = nil
local schedule_handleMessages_id = nil

local function reloadMap(cityId,cityList)
    --加载滚动地图
    local scrollMap = tolua.cast(LayerHomePage["scrollMap"],"CCScrollView")
    local originOffset = scrollMap:getContentOffset() 
    --移除地图内容
    scrollMap:removeAllChildrenWithCleanup(true)

    --重新生成地图
    local viewSize = scrollMap:getViewSize()
    
    scrollMap:setScale(1.0)
    scrollMap:ignoreAnchorPointForPosition(true)
    local nodeToScroll = CCNode:create()
    scrollMap:setContainer(nodeToScroll)
    
    local spriteMapHeight = 0
    --加载地图
    for i = 0,12 do
        local spriteMap =  CCSprite:create("UI/map1.png")
        local spriteMapSize = spriteMap:getContentSize()
        spriteMapHeight = spriteMapSize.height
        
        spriteMap:setAnchorPoint(ccp(0,0))
        spriteMap:setPosition(ccp(0,spriteMapHeight*i))
        nodeToScroll:addChild(spriteMap)
    end
    
    function clickedAtMapItem(event,pSender)
        if pSender ~= nil then
            local btnSender = tolua.cast(pSender,"CCControl")
            layer:addChild(battleInfoLayer(GameInfo.mapConfig:getMapItemInfoByLevel(btnSender:getTag())))
        end
    end
    
    --加载副本
    local totalLevel = GameInfo.mapConfig:getCount()
    local finishedLevel = table.getn(cityList)
    local yOffset = 0 
    if totalLevel > 0 then
        for i = 0 ,totalLevel-1 do
            local mapItemInfo = GameInfo.mapConfig:getMapItemInfoByLevel(i+1000)
            if mapItemInfo ~= nil then
                local mapItem = MapItem.create()
                nodeToScroll:addChild(mapItem:getUI())
                mapItem:getUI():setPosition(ccp(200,100*i+100))
                mapItem:getUI():addHandleOfControlEvent(clickedAtMapItem,CCControlEventTouchUpInside)
                mapItem:getUI():setTag(i+1000)
                mapItem:getUI():setTitleForState(CCString:create(mapItemInfo.nickname),CCControlStateNormal)
                mapItem:getUI():setTitleTTFSizeForState(18, CCControlStateNormal)
                
                if i ==finishedLevel + 1 then
                    yOffset = 100*i+100
                end
                
                if i >= finishedLevel + 1 then
                    mapItem:getUI():setVisible(false)
                end
            end
        end
    end
    
    if yOffset > 300 then
        yOffset = yOffset - 300
    end

    scrollMap:setContentOffset(originOffset)
    scrollMap:setContentSize(CCSizeMake(viewSize.width, spriteMapHeight*13))
    scrollMap:setContentOffsetInDuration(ccp(0,-yOffset),0.5)
    scrollMap:updateInset()
    scrollMap:setDirection(kCCScrollViewDirectionVertical)
    scrollMap:setClippingToBounds(true)
    scrollMap:setBounceable(true)
end

--处理4500消息
local function handleMsg4500()
    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),4500)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
        local data = jsonData.data

        if bResult then
            cclog("recv msg4500 ok , parsing...")
            local cityId = data.cityid
            local cityList = data.citylist
            
            UserInfo.Task.cityid = cityId
            UserInfo.Task.citylist = cityList
            
            --重新加载地图
            reloadMap(cityId,cityList)
        else
            cclog("handleMsg4500 error")
        end
        LoadingLayer.dismiss()
    end

end

--处理4501消息 -- (战斗)
local function handleMsg4501()
    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),4501)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
        local data = jsonData.data

        if bResult then
            cclog("recv msg4501 ok , parsing...")
            --跳转到战斗界面
            local scene = CCScene:create()
            scene:addChild(battleLayer(jsonData))
            CCDirector:sharedDirector():pushScene(scene)
        else
            cclog("handleMsg4501 error:"..message)
            CCMessageBox(message,"")
        end
    end
end

local function updateUserInfo()
    --更新界面信息
    local lblUserLevel = tolua.cast(LayerHomePage["labelUserLevel"],"CCLabelTTF") 
    local lblUserName = tolua.cast(LayerHomePage["labelUserName"],"CCLabelTTF") 
    local lblUserCoins = tolua.cast(LayerHomePage["labelCoins"],"CCLabelTTF") 
    local lblUserDiamonds = tolua.cast(LayerHomePage["labelDiamonds"],"CCLabelTTF") 
    local lblUserGas = tolua.cast(LayerHomePage["labelGas"],"CCLabelTTF") 
    local lblUserPower = tolua.cast(LayerHomePage["labelPower"],"CCLabelTTF") 
       
    lblUserLevel:setString(UserInfo.level)
    lblUserName:setString(UserInfo.name)
    lblUserCoins:setString(UserInfo.coin)
    lblUserDiamonds:setString(UserInfo.yuanbao)
    lblUserGas:setString(UserInfo.gas)
    lblUserPower:setString(UserInfo.power)

end

--处理105消息 , 更新顶部个人信息状态
local function handleMsg105()
    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),105)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
        local data = jsonData.data

        if bResult then
            
             --处理新角色信息
            local uName = data.rolename
            local uPower = data.tili
            local uCid = data.characterId
            local uGas = data.huoli
            local uLevel = data.level
            local uProfession = data.profession
            local uMaxExp = data.maxexp
            local uExp = data.exp
            local uCoin = data.coin
            local uYuanbao = data.gold
            
            UserInfo.name = uName
            UserInfo.power = uPower
            UserInfo.characterId = uCid
            UserInfo.gas = uGas
            UserInfo.level = uLevel
            UserInfo.profession = uProfession
            UserInfo.maxexp = uMaxExp
            UserInfo.exp = uExp
            UserInfo.coin = uCoin
            UserInfo.yuanbao = uYuanbao
            
            updateUserInfo()
        else
            cclog("handleMsg 105 error")
        end
    end

end


--处理消息
local function handleMessages()
    if NetInfo.socketManager ~= nil then
        --更新地图
        handleMsg4500()
        --更新面部状态
        handleMsg105()
        --处理战斗
        handleMsg4501()
    end
end

local function updateAll()
    --加载中...   
    LoadingLayer.show()
    --构造json
    --发送获取地图信息消息
    local msgMapInfo ={}
    msgMapInfo["characterId"] = UserInfo.characterId
    msgMapInfo["index"] = 0
    local jsonMapInfo = cjson.encode(msgMapInfo)
    NetInfo.socketManager:sendMessage(jsonMapInfo,4500)

    --发送更新个人信息消息
    local msgUserInfo = {}
    msgUserInfo["characterId"] = UserInfo.characterId
    local jsonUserInfo = cjson.encode(msgUserInfo)
    NetInfo.socketManager:sendMessage(jsonUserInfo,105)

    --更新个人信息
    updateUserInfo()
end

local function onEnter()
    updateAll()
    schedule_handleMessages_id = scheduler:scheduleScriptFunc(handleMessages, 0.1, false)
end

local function onExit()
     scheduler:unscheduleScriptEntry(schedule_handleMessages_id)
end

function homePageLayer()

    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_homepage.ccbi",proxy,LayerHomePage)
    layer = tolua.cast(node,"CCLayer")

    --node Events
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end
    layer:registerScriptHandler(onNodeEvent)
    
    --更新所有信息
    updateAll()
    
    return layer
end

--------------------------------------------------------------
--按钮事件
local function clickedAtEmployButton()
    require "lua/Layers/Layer_EmployMain"
    layer:addChild(employMainLayer())
end

LayerHomePage["clickedAtEmployButton"] = clickedAtEmployButton 






