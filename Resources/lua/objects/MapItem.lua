require "lua/extern"

MapItem = {}
MapItem.__index = MapItem

function MapItem.create()
    local mapItem = {}          
    setmetatable(mapItem,MapItem) 
    
    --初始化界面
    local pBackgroundButton = CCScale9Sprite:create("UI/zjm_building_1.png")
    local pBackgroundHighlightedButton = CCScale9Sprite:create("UI/zjm_building_2.png")

    local pTitleButton = CCLabelTTF:create("hello", "Marker Felt", 30)
    pTitleButton:setColor(ccc3(159, 168, 176))

    local pButton = CCControlButton:create(pTitleButton, pBackgroundButton)
    pButton:setBackgroundSpriteForState(pBackgroundHighlightedButton, CCControlStateHighlighted)
    pButton:setTitleColorForState(ccc3(255,255,255), CCControlStateHighlighted) 
    pButton:setAdjustBackgroundImage(false)
    pButton:setZoomOnTouchDown(false)
    mapItem.UI = pButton
    
    return mapItem
end

function MapItem:getUI()
    return self.UI
end
