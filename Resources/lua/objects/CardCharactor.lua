require "lua/extern"

CardCharactor = {}
CardCharactor.__index = CardCharactor

function CardCharactor.create(cardFileName)
    local cCharactorItem = {}          
    setmetatable(cCharactorItem,CardCharactor)
    
    --初始化界面
    local nodeUI = CCNode:create()
    
    --添加卡片头像
    local spriteCard = CCSprite:create(cardFileName)
    nodeUI:addChild(spriteCard)

    --添加血条
    local cardSize = spriteCard:getContentSize()
    local progressBlood = CCProgressTimer:create(CCSprite:create("UI/PK_blood.png"))
    progressBlood:setType(kCCProgressTimerTypeBar)
    progressBlood:setMidpoint(CCPointMake(0, 0))
    progressBlood:setBarChangeRate(CCPointMake(1, 0))
    progressBlood:setPercentage(100)
    progressBlood:setPosition(ccp(0,cardSize.height/2-8))
	nodeUI:addChild(progressBlood)
    
    --添加一个边框
    local spriteBorder = CCSprite:create("UI/common_qs_bkg_1.png")
    nodeUI:addChild(spriteBorder)
 
    --设置UI字段
    cCharactorItem.UI = nodeUI
    cCharactorItem.Card = spriteCard
    cCharactorItem.Blood = progressBlood
    
    --属性字段
    cCharactorItem.chaBattleId = nil
    cCharactorItem.chaName = nil
    cCharactorItem.chaPos = nil
    cCharactorItem.chaLevel = nil
    cCharactorItem.chaDirection = nil
    cCharactorItem.chaIcon = nil
    cCharactorItem.chaCurrentHp = nil
    cCharactorItem.chaTotalHp = nil
    cCharactorItem.chaId = nil

    return cCharactorItem
end

function CardCharactor:getUI()
    return self.UI
end

function CardCharactor:getBlood()
    return self.Blood
end

function CardCharactor:getChaBattleId()
    return self.chaBattleId
end

function CardCharactor:setChaBattleId(newValue)
    self.chaBattleId = newValue
end

function CardCharactor:getChaName(newValue)
    return self.chaName
end

function CardCharactor:setChaName(newValue)
    self.chaName = newValue
end

function CardCharactor:getChaPos()
    return self.chaPos
end

function CardCharactor:setChaPos(newValue)
    self.chaPos = newValue
end

function CardCharactor:getChaLevel()
    return self.chaLevel
end

function CardCharactor:setChaLevel(newValue)
    self.chaLevel = newValue
end

function CardCharactor:getChaDirection()
    return self.chaDirection
end

function CardCharactor:setChaDirection(newValue)
    self.chaDirection = newValue
end

function CardCharactor:getChaIcon()
    return self.chaIcon
end

function CardCharactor:setChaIcon(newValue)
    self.chaIcon = newValue
end

function CardCharactor:getChaCurrentHp()
    return self.chaCurrentHp
end

function CardCharactor:setChaCurrentHp(newValue)
    self.chaCurrentHp = newValue
end

function CardCharactor:getChaTotalHp()
    return self.chaTotalHp 
end

function CardCharactor:setChaTotalHp(newValue)
    self.chaTotalHp = newValue
end

function CardCharactor:getChaId()
    return self.chaId 
end

function CardCharactor:setChaId(newValue)
    self.chaId = newValue
end


