require "lua/extern"

Object_FriendCell = Object_FriendCell or {}
Object_FriendCell.__index = Object_FriendCell

ccb["Object_FriendCell"] = Object_FriendCell

function Object_FriendCell.create()
    local object_friendcell = {}          
    setmetatable(object_friendcell,Object_FriendCell)
    
    local  proxy = CCBProxy:create()
    local node = CCBuilderReaderLoad("CCBObject/Object_FriendCell.ccbi",proxy,Object_FriendCell)
    local nodeUI = tolua.cast(node,"CCNode")
    local labelLevel = tolua.cast(Object_FriendCell["labelLevel"],"CCLabelTTF")
    local labelName = tolua.cast(Object_FriendCell["labelName"],"CCLabelTTF")
    local labelSkill = tolua.cast(Object_FriendCell["labelSkill"],"CCLabelTTF")
    local labelMoney = tolua.cast(Object_FriendCell["labelMoney"],"CCLabelTTF")

    --设置信息
    object_friendcell.Node = nodeUI
    object_friendcell.LabelLevel = labelLevel
    object_friendcell.LabelName = labelName
    object_friendcell.LabelSkill = labelSkill
    object_friendcell.LabelMoney = labelMoney

    
    object_friendcell.level = nil
    object_friendcell.price = nil
    object_friendcell.bEmploy = false
    object_friendcell.chaid = nil
    object_friendcell.rank = nil
    object_friendcell.rolename = nil
    object_friendcell.skill = nil

    return object_friendcell
end

function Object_FriendCell:setStringLevel(newValue)
    self.LabelLevel:setString(newValue)
end

function Object_FriendCell:setStringName(newValue)
    self.LabelName:setString(newValue)
end

function Object_FriendCell:setStringSkill(newValue)
    self.LabelSkill:setString(newValue)
end

function Object_FriendCell:setStringMoney(newValue)
    self.LabelMoney:setString(newValue)
end







