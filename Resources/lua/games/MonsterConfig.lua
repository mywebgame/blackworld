require "lua/extern"
require "lua/data/MonsterItemInfo"

MonsterConfig = {}
MonsterConfig.__index = MonsterConfig

function MonsterConfig:loadData()

    local mfContent = CCString:createWithContentsOfFile(CCFileUtils:sharedFileUtils():fullPathForFilename("games/monster.json")):getCString()
    local jsonData = cjson.decode(mfContent)
    local dataValue = jsonData.data

    local miNumbers = table.getn(dataValue)
    
    for i = 1, miNumbers ,1 do
        local monsterInfoData = dataValue[i]
        
        local monsterItemInfo = MonsterItemInfo.create() 
        monsterItemInfo.id = monsterInfoData.id
        monsterItemInfo.nickname = monsterInfoData.nickname
        self.monsterItems[i]=monsterItemInfo
    end
    

end

function MonsterConfig.create()
    local mstCfg = {}          
    setmetatable(mstCfg,MonsterConfig) 
    
    mstCfg.monsterItems={} 
    return mstCfg
end

function MonsterConfig:getMonsterItemInfoById(levelId)
    local monsterCount = table.getn(self.monsterItems)
    local monsterRet = nil
    
    if monsterCount > 0 then
        for i=1, monsterCount, 1 do
            local monsterTest =self.monsterItems[i]
            if monsterTest.id == levelId then
                monsterRet = monsterTest
                break
            end
        end
    end
    return monsterRet
end












