require "lua/extern"
require "lua/data/MapItemInfo"
require "lua/data/MonsterItemInfo"

MapConfig = {}
MapConfig.__index = MapConfig

function MapConfig:loadData()
    local mfContent = CCString:createWithContentsOfFile(CCFileUtils:sharedFileUtils():fullPathForFilename("games/map.json")):getCString()
    local jsonData = cjson.decode(mfContent)
    local mapInfos = jsonData.data
    
    local miNumbers = table.getn(mapInfos)
    
    cclog("........副本信息........")
    for i = 1, miNumbers ,1 do
        local mpInfoData = mapInfos[i]
        
        local mapItemInfo = MapItemInfo.create() 
        
        mapItemInfo.bid = mpInfoData.id
        mapItemInfo.nickname = mpInfoData.name
        mapItemInfo.desc = mpInfoData.desc
        mapItemInfo.exp = mpInfoData.exp
        mapItemInfo.coin = mpInfoData.coin
        mapItemInfo.itemId = mpInfoData.dropicon
                
        --加载怪物信息
        local listMonster = mpInfoData.mconfig
        
        local monsterNumber =  table.getn(listMonster)
        local tableMonsters = {}
        
        for j = 1,monsterNumber,1 do
            local monsterId = listMonster[j]
            local monsterItemInfo = GameInfo.monsterConfig:getMonsterItemInfoById(monsterId)
            
            if monsterItemInfo ~= nil then
                table.insert(tableMonsters,monsterItemInfo)
            else
                cclog("怪物 id 不存在 "..monsterId)
            end
        end
        
        local levelMonsterCount = #tableMonsters
        local enemyDescrip = nil

        if levelMonsterCount == 1 then
            local monsterItemInfo1 = tableMonsters[1]
            local enemyDescrip1 = monsterItemInfo1.nickname
            enemyDescrip = enemyDescrip1
        elseif levelMonsterCount == 2 then
            local monsterItemInfo1 = tableMonsters[1]
            local monsterItemInfo2 = tableMonsters[2]
            local enemyDescrip1 = monsterItemInfo1.nickname
            local enemyDescrip2 = monsterItemInfo2.nickname
            enemyDescrip = enemyDescrip1 .. "|" .. enemyDescrip2
        elseif levelMonsterCount == 3 then
            local monsterItemInfo1 = tableMonsters[1]
            local monsterItemInfo2 = tableMonsters[2]
            local monsterItemInfo3 = tableMonsters[3]
            local enemyDescrip1 = monsterItemInfo1.nickname
            local enemyDescrip2 = monsterItemInfo2.nickname
            local enemyDescrip3 = monsterItemInfo3.nickname
            enemyDescrip = enemyDescrip1 .. "|" .. enemyDescrip2 .. "|" .. enemyDescrip3
        elseif levelMonsterCount >= 4 then
            local monsterItemInfo1 = tableMonsters[1]
            local monsterItemInfo2 = tableMonsters[2]
            local monsterItemInfo3 = tableMonsters[3]
            local monsterItemInfo4 = tableMonsters[4]
            local enemyDescrip1 = monsterItemInfo1.nickname
            local enemyDescrip2 = monsterItemInfo2.nickname
            local enemyDescrip3 = monsterItemInfo3.nickname
            local enemyDescrip4 = monsterItemInfo4.nickname
            enemyDescrip = enemyDescrip1 .. "|" .. enemyDescrip2 .. "|" .. enemyDescrip3 .. "|" .. enemyDescrip4
        end
        
        if enemyDescrip ~= nil then
            cclog(enemyDescrip)
        end
        mapItemInfo.enemydesc = enemyDescrip
        self.mapItems[i] = mapItemInfo
    end
    
    cclog("副本总数:"..table.getn(self.mapItems))
end

function MapConfig.create()
    local mapCfg = {}          
    setmetatable(mapCfg,MapConfig) 
    
    mapCfg.mapItems={} 
    return mapCfg
end

function MapConfig:getMapItemInfoByLevel(levelId)
    local mapItemCount = table.getn(self.mapItems)
    local mapItemRet = nil
    
    if mapItemCount > 0 then
        for i=1, mapItemCount, 1 do
            local mapItemTest =self.mapItems[i]

            if mapItemTest.bid == levelId then
                mapItemRet = mapItemTest
                break
            end
        end
    end
    return mapItemRet
end

function MapConfig:getCount()
    return table.getn(self.mapItems)
end
