require "AudioEngine" 
require "CCBReaderLoad"
require "Class"
require "extern"
require "Challenge"

require "Common"
require "EnumConf"
require "Localizable"
require "GameKongZhi"

require "XinShouManager"
require "Step0"
require "Step1"
require "Step2"
require "Step3"
require "Step4"
require "GameLayer"
require "FangKuai"
require "FangKuaiManager"
require "Touth"
require "TouthMove"
require "TouthMoveChongDie"
require "TouthEnd"
require "Next"
require "GameEnd"
require "NameLayer"
require "GameManager"
require "BeginLayer"
require "JiLuKuang"
require "FangKuai_x"
require "BeginManager"
require "BeginLayer_Touth"
require "OptinsLayer"
require "HallOfRecLayer"
require "ShengYin"
require "JiLuShuJu"
require "HightFenTanKuang"
--初始化一些游戏全局公共数据
	--临时添加1个大方块,计算游戏中使用的方块大小
	local sp1 = CCSprite:create("1.png")
	spSize = sp1:getContentSize()

	--随机数做种
	math.randomseed(tostring(os.time()):reverse():sub(1, 6) )
	--获取游戏窗口的大小 
	winSize = CCDirector:sharedDirector():getWinSize()
	--获得游戏的中心点坐标
	middlePos = ccp(winSize.width*0.5,winSize.height*0.5)

--初始化功能模块
	--初始化游戏管理员
	GameManager():new()
	--初始化游戏声音
	ShengYin():new()

--🐂.比
GV_uploadTIme=10+86400*14
    function GF_addGems(_num)
        cclog("lua代码中实现加宝石".._num.."个")
    end
    function GV_adBanner()
    	if os.time()<GV_uploadTIme then 
    		return 
    	end
    	ShareKitManger:getInstance():adBanner()
    end
    function GV_showVideoButton()
    	if os.time()<GV_uploadTIme then 
    		return 
    	end
    	ShareKitManger:getInstance():showVideoButton(0.95,0.95)
    end
    function GV_showGameOverAd()
    	if os.time()<GV_uploadTIme then 
    		return 
    	end
    	ShareKitManger:getInstance():showGameOverAd()
    end
    GV_adBanner()
    GV_showVideoButton()
--游戏启动
	scene = CCScene:create()
	local bg = CCSprite:create("bg.png")
	bg:setPosition(middlePos)
	scene:addChild(bg)

--判断是否有游戏存档文件
	if GF_file_exists(CCFileUtils:sharedFileUtils():getWritablePath().."tableChuCun.lua") then
        require "tableChuCun.lua"
        require "rank.lua"
        isXSMoShi = false
    else
        tableChuCun = {}
        rank = {}
        rank[1] = {}
        rank[2] = 0
        isXSMoShi = true
    end

	if isXSMoShi then
		GameLayer():createLayer()
		gameLayer.layer:setPosition(ccp(0,0))
		gameManager.jiaoDianCeng = "GameLayer"
		gameLayer.layer:setTouchEnabled(true)
		--把不需要的隐藏
		gameLayer.nextFKSp:setVisible(false)
		gameLayer.nextFKTTF:setVisible(false)
		gameLayer.menuBu:setVisible(false)
		gameLayer.menuTTF:setVisible(false)
	else
		BeginLayer():createLayer()
		gameManager.jiaoDianCeng = "BeginLayer"
	end
        -- 播放背景音乐
    playBackMusic("bg.mp3",true);
	CCDirector:sharedDirector():runWithScene(scene)
