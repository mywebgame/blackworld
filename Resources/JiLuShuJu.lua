function FangKuaiManager:jiLuShuJu()
	local tempRank = {}
	for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			local fenShu = fk.fenShu
			if fenShu >= 3 then
				if tempRank[fenShu] == nil then
					tempRank[fenShu] = 1
				else
					tempRank[fenShu] = tempRank[fenShu] + 1
				end
			end
		end
	end

	table.foreach(tempRank, function(i, v) 
		local key = math.log (i/3,2) + 1
		if rank[1][key] ~= nil then
			if rank[1][key][2] < v then
				rank[1][key][2] = v
			end
		else
			rank[1][key] = {}
			rank[1][key][1] = i
			rank[1][key][2] = v
		end
	end)
end

function FangKuaiManager:endJuLu()
cclog("self.endGameJiFen = "..self.endGameJiFen)
	--检查是否突破最高分
	if fangKuaiManager.endGameJiFen > rank[2] then
		rank[2] = self.endGameJiFen
		sentMaxDistance(fangKuaiManager.endGameJiFen)
	end

	--游戏记录储存
	GF_saveTableToFile(rank,"rank",CCFileUtils:sharedFileUtils():getWritablePath().."rank.lua")
end
