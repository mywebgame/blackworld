XinShouManager=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                         for i,v in pairs(agr) do
                            obj[i] = v
                         end
                    end
                end )

function XinShouManager:new()
	xinShouManager = self
	--这个属性用来控制移动时是否已经判断出移动方向
	self.isMoveing = false
	--记录当前的步骤
	self.step = 0
	--步骤开始
	self:step0()
	--记录步数
	self.bu = 0
end

--这个方法根据传入的数字给方块随机分配一个位置
function XinShouManager:kaPianSuiJiWeiZhi(tempfenShu)
	local weiZhi = math.floor( math.random() * HANG * LIE + 1)
	local X = math.ceil(weiZhi/LIE)
	local Y = weiZhi - (X-1)*LIE

	if fangKuaiManager.fangKuaiTable[X][Y].fenShu == 0 then
		fangKuaiManager.fangKuaiTable[X][Y].fenShu = tempfenShu
	else
		self:kaPianSuiJiWeiZhi(tempfenShu)
	end
end

--检测当前步骤是否完成
function XinShouManager:jianCe_step()
	self.bu = self.bu + 1
	if self.step == 0 then
		self:jianCe_step0()
	end

	if self.step == 1 then
		self:jianCe_step1()
	end

	if self.step == 2 then
		self:jianCe_step2()
	end

	if self.step == 3 then
		self:jianCe_step3()
	end

	if self.step == 4 then
		self:jianCe_step4()
	end

	if self.step == 5 then
		--游戏开始
		isXSMoShi = false
		--生成下一个方块
        fangKuaiManager:nextFangKuaiRandom()
        local function funC( ... )
			gameLayer.mAnimationManager:runAnimationsForSequenceNamed("jieSuanZou")
        end

        GF_performWithDelay(1,funC)
	end


end