Localizable = {}
if GV_languageType == GV_LanguageTypeEnum.kLanguageChinese then
     --cclog("CurrentLanguage is Chinese")
     require "Localizable_ch";
else
     --cclog("CurrentLanguage is English")
     require "Localizable_en";
end

function GF_formatString( ... )
     if not GV_DEBUG_MODE then
          return string.format(...)
     else
          local flag, responseStr = pcall(string.format, ...)
          if flag then
               return string.format(...)
          else
               GF_trace(responseStr)
          end
     end
end
function string:split(delimiter)

     local result = {}
     local from  = 1
     local delim_from, delim_to = string.find( self, delimiter, from  )
     while delim_from do
          table.insert( result, string.sub( self, from , delim_from-1 ) )
          from = delim_to + 1
          delim_from, delim_to = string.find( self, delimiter, from  )
     end
     table.insert( result, string.sub( self, from  ) )
     return result
end
function Localizable.valueFor(key, ...)
     local keyArr=key:split('[.]')
     if table.getn(keyArr) == 2 then
         value=Localizable[keyArr[1]][keyArr[2]]
     else
          value=Localizable[key]
     end
     value = value or key
     newValue = GF_formatString(value, ...)
     return newValue;
end
GF_localString = Localizable.valueFor
