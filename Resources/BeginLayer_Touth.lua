function onBeginLayerTouch(eventType, x, y)
	if gameManager.jiaoDianCeng == "BeginLayer" then
	    if eventType == "began" then
	        return onBeginLayerTouchBegan(x, y)
	    end
	    if eventType == "moved" then
	        return onBeginLayerTouchMoved(x, y)
	    end
	    if eventType == "ended" then
	        return onBeginLayerTouchEnded(x, y)
	    end
	end
end

function onBeginLayerTouchBegan(x, y)
	BeginPos = {x = x, y = y}
	--保存当前的坐标,下一帧判断使用
	moveLastPos = ccp(x,y)
    return true
end

function onBeginLayerTouchMoved(x, y)
	if beginManager.isMoveTop ~= 1 then
		-- 正在执行上下拖动动画
		return;
	end
	--根据当前的点击位置与上一帧对比,获得移动的距离
	moveChangDu = ccp(moveLastPos.x-x,moveLastPos.y-y)
	--保存当前帧的坐标,下一帧判断使用
	moveLastPos = ccp(x,y)
	--拖动方框
	beginManager:moveing()
    return true
end

function onBeginLayerTouchEnded(x, y)
	if beginManager.isMoveTop ~= 1 then
		-- 正在执行上下拖动动画
		beginManager.isMoveTop = 1;
		return;
	end
	--判断移动了多少
	local moveJuLi = x - BeginPos.x
	if moveJuLi > 40 then
		--向右移动,判断是会越界
		if  beginManager.zhong < table.getn(beginManager.tuoDongNode) then
			beginManager.zhong = beginManager.zhong + 1
			playEffect("whiff.wav")
		end
	elseif moveJuLi < -40 then
		--向左移动,判断是会越界
		if beginManager.zhong > 1 then
			beginManager.zhong = beginManager.zhong - 1
			playEffect("whiff.wav")
		end
	end

	beginManager:endMove()
	cclog("----------------BeginLayer,华丽的分割线----------------")
    return true
end

--拖动时移动的函数
function BeginManager:moveing()
    for i=1,table.getn(beginManager.tuoDongNode) do
    	local tuoDongNode = beginManager.tuoDongNode[i].layer
    	local nowPosX,nowPosY = tuoDongNode:getPosition()
    	tuoDongNode:setPosition(ccp(nowPosX - moveChangDu.x,nowPosY))
    end
end

--拖动结束时调用的函数
function BeginManager:endMove()
	local kuangPos = beginManager.tuoDongNode[beginManager.zhong].middleNode:convertToWorldSpaceAR(ccp(0,0)).x
	for i=1,table.getn(beginManager.tuoDongNode) do
		local move = CCEaseExponentialOut:create(CCMoveBy:create(0.3, ccp( middlePos.x - kuangPos, 0)))
		beginManager.tuoDongNode[i].layer:runAction(move)
	end
end