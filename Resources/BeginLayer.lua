BeginLayer=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                        for i,v in pairs(agr) do
                            obj[i] = v
                        end
                    end
                end )

function BeginLayer:createLayer()
	cclog("BeginLayer - 被创建")
	self.layer = CCBuilderReaderLoad("BeginLayer.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
	self.layer:setPosition(ccp(0,0))
	beginLayer = self
	scene:addChild(self.layer)

	--初始化游戏场景
  	self:init()
end

-- 排名
function BeginLayer:leader()
    -- 显示排行榜
    showLeaderboard(rank[2]);
    --
end

function BeginLayer:deleteSelf()
	cclog("BeginLayer - 被删除")
	beginLayer.layer:removeFromParentAndCleanup(true)
	beginLayer = nil
end

function BeginLayer:init()
	--初始化BeginLayer管理员
	BeginManager():new()
    
    --获取记录的
    local jiLuNum = table.getn(tableChuCun)
    cclog("记录创建中,目前有%d个记录",jiLuNum)
    for i=1,jiLuNum do
    	beginLayer:makeOneJiLvKuang(i)
    end

    --多语言适配
    self.startTTF:setString(Localizable.BeginLayer_retry)

    --创建记录右边的可拖动的控件
    local optins = OptinsLayer()
    optins:createLayer(2)
    local hallOfRec = HallOfRecLayer()
    hallOfRec:createLayer(1)

    --窗口进行排序
    function sortFunc (a, b)
        return a.paiMing < b.paiMing
    end
    table.sort(beginManager.tuoDongNode,sortFunc)

    --防止没有存档时,焦点窗口溢出
    if jiLuNum == 0 and BEGINLAYERTINGZHIKONGJIAN > JILUYOUBIANDEKONGJIANSHU then
        BEGINLAYERTINGZHIKONGJIAN = JILUYOUBIANDEKONGJIANSHU
        beginManager.zhong = JILUYOUBIANDEKONGJIANSHU
    end

    --根据游戏控制中得设置,对窗口位置进行调整
    local kuangPos = beginManager.tuoDongNode[BEGINLAYERTINGZHIKONGJIAN].middleNode:convertToWorldSpaceAR(ccp(0,0)).x
    local xCha = winSize.width * 0.5 - kuangPos
    for i=1,table.getn(beginManager.tuoDongNode) do
    	local tuoDongNode = beginManager.tuoDongNode[i].layer
    	local nowPosX,nowPosY = tuoDongNode:getPosition()
    	tuoDongNode:setPosition(ccp(nowPosX + xCha,nowPosY))
    end

    --注册点击事件
    self.layer:setTouchEnabled(true)
    self.layer:unregisterScriptTouchHandler()
    self.layer:registerScriptTouchHandler(onBeginLayerTouch,false,-1,false)
end

--生成1个可以拖动的记录框
function BeginLayer:makeOneJiLvKuang(i)
	local kuang = JiLuKuang()
	kuang:createLayer(i)
	kuang:init()
end

--按钮的回调函数,进入游戏
function BeginLayer:gameIn()
    playEffect("whiff.wav")
    if not gameManager.isGameing then
        GameLayer():createLayer()
    end
    gameManager.jiaoDianCeng = "GameLayer"
    gameLayer.layer:setTouchEnabled(true)
    --锁定当前按钮
    beginLayer:butOffOn(false)
    gameLayer:butOffOn(false)
    --播放入场动画
    local move = CCEaseExponentialOut:create(CCMoveBy:create(TIAOZHUANCHANGJINGTIME, ccp(0,-winSize.height)))
    gameLayer.layer:runAction(move)

    local function funC()
        gameLayer:butOffOn(true)
        self:deleteSelf() 
        --gameManager:shuaXinZuiGaoFen()
    end
    --播放离开动画
    local move = CCEaseExponentialOut:create(CCMoveBy:create(TIAOZHUANCHANGJINGTIME, ccp(0,-winSize.height)))
    self.layer:runAction(move)
    GF_performWithDelay(TIAOZHUANCHANGJINGTIME,funC)
end

--播放BeginLayer入场动画
function BeginLayer:layerIn()
	--不知道为什么要这样,反正不这样从游戏页面回来的时候,X是2个值,Y是空的
	self.layer:setTouchEnabled(false)
	self.layer:setTouchEnabled(true)

    self.layer:setPosition(ccp(0,-winSize.height))
    local move = CCEaseExponentialOut:create(CCMoveBy:create(TIAOZHUANCHANGJINGTIME, ccp(0,winSize.height)))
    self.layer:runAction(move)

    local function funC()
        gameLayer:butOffOn(true)
        beginLayer:butOffOn(true)
        gameManager.jiaoDianCeng = "BeginLayer"
    end
    GF_performWithDelay(TIAOZHUANCHANGJINGTIME,funC)
end

function BeginLayer:butOffOn(is)
    beginLayer.kaishi:setEnabled(is)
    beginLayer.butgc:setEnabled(is)
end
