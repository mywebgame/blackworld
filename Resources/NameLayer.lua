NameLayer=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                        for i,v in pairs(agr) do
                            obj[i] = v
                        end
                    end
                end )

function NameLayer:createLayer()
	cclog("名字层 - 被创建")
	self.layer = CCBuilderReaderLoad("NameLayer.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
	self.layer:setPosition(ccp(0,-winSize.height))
	nameLayer = self
	scene:addChild(self.layer)

	self:init()
end

function NameLayer:deleteSelf()
	cclog("名字层 - 被删除")
	nameLayer.layer:removeFromParentAndCleanup(true)
	nameLayer = nil
end

function NameLayer:init()
    --更新CCB上假的输入文字
    self.jiaTTF:setString(NAMELAYERMORENNAME)

    self:layerIn()
end

function NameLayer:editBoxTextEventHandle( ... )
    gameManager.jiaoDianCeng = ""
    playEffect("whiff.wav")
    fangKuaiManager.endGameName = NAMELAYERMORENNAME
    nameLayer:layerOut()
    gameManager:chuCunGame()
end

function NameLayer:shuRuKuang()
    local boxSize = nameLayer.inputNode:getContentSize()
    local boxPosX,boxPosY = nameLayer.inputNode:getPosition()

    local EditName
    local function editBoxTextEventHandle(strEventName,pSender)
        if strEventName == "ended" then
            playEffect("whiff.wav")
            if EditName:getText() == "" then
                fangKuaiManager.endGameName = NAMELAYERMORENNAME
            else
                fangKuaiManager.endGameName = EditName:getText()
            end
            nameLayer:layerOut()
            gameManager:chuCunGame()
            cclog("fangKuaiManager.endGameName = "..fangKuaiManager.endGameName)
        end
    end

    --创建输入框
    EditName = CCEditBox:create(boxSize, CCScale9Sprite:create())
    EditName:setAnchorPoint(ccp(0.5,1))
    EditName:setPosition(ccp(boxPosX,boxPosY))
    EditName:setFontName("Sketch Rockwell.ttf")
    EditName:setFontSize(55)
    EditName:setFontColor(ccc3(255,0,0))
    EditName:setPlaceHolder(NAMELAYERMORENNAME)
    EditName:setPlaceholderFontColor(ccc3(255,0,0))
    EditName:setMaxLength(ENDGAMENAMEZIFUSHU)
    EditName:registerScriptEditBoxHandler(editBoxTextEventHandle)
    self.bgSp:addChild(EditName)
    --删除CCB上创建的那个假的显示文字
    self.jiaTTF:removeFromParentAndCleanup(true)
end


function NameLayer:layerIn()
    local function funC()
        --创建输入框
        nameLayer:shuRuKuang()

        --结束键盘锁定
        gameManager.jiaoDianCeng = "GameLayer"
    end
    local move = CCEaseExponentialOut:create(CCMoveBy:create(TIAOZHUANCHANGJINGTIME, ccp(0,winSize.height)))
    local arr = CCArray:create()
    arr:addObject(move)
    arr:addObject(CCCallFunc:create(funC))
    self.layer:runAction( CCSequence:create(arr))
end

function NameLayer:layerOut()

    local function funC1()
        local function funC()
            self:deleteSelf() 
            --GameLayer删除
            gameLayer:gameOut()
            gameLayer.liZiNode:removeAllChildrenWithCleanup(true)
        end

        --播放离开动画
        local move = CCEaseExponentialIn:create(CCMoveBy:create(TIAOZHUANCHANGJINGTIME, ccp(0,-winSize.height)))

        local arr = CCArray:create()
        arr:addObject(move)
        arr:addObject(CCCallFunc:create(funC))
        self.layer:runAction( CCSequence:create(arr))
    end
    GF_performWithDelay(TIAOZHUANCHANGJINGTIME,funC1)
end