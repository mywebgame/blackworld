ChallengeSp = ObjClass("MainLayer",
    function()
        -- 返回ccb对象
        local sprite = CCBuilderReaderLoad("challenge.ccbi",CCBProxy:create(),ChallengeSp)
        sprite = tolua.cast(sprite,"CCSprite")
        return sprite;
    end
)

-- 创建layer
function ChallengeSp:createSp()
    local sp = self:new();
    return sp;
end

-- 创建时候调用的方法 相当于init
function ChallengeSp:ctor()

end

-- 赋值对象
function ChallengeSp:setObj(challengeName,challengeNum)
	self.challengeNum:setString(challengeNum);
	self.challengeName:setString(challengeName);
end

