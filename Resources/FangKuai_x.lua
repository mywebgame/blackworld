FangKuai_x=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                         for i,v in pairs(agr) do
                            obj[i] = v
                         end
                    end
                end )

function FangKuai_x:create(kuang)
	self.layer = CCBuilderReaderLoad("FangKuai_x.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
	self.layer:setPosition(ccp(0,0))
	kuang:addChild(self.layer)
end

--消除方块
function FangKuai_x:deleteSelf()
	--方块的精灵
	self.layer:removeFromParentAndCleanup(true)
end

function FangKuai_x:init(tempTable,i,j,zeroX,zeroY)
	local fenShu = tempTable[1][i][j]
    if fenShu == 1 then
        self.sp:setTexture(CCSprite:create("1-x.png"):getTexture())
    elseif fenShu == 2 then
        self.sp:setTexture(CCSprite:create("2-x.png"):getTexture())
    else
        self.sp:setTexture(CCSprite:create("3-x.png"):getTexture())
    end

    if fenShu >= 3 then
        self.ttf:setString(fenShu)
    else
        self.ttf:removeFromParentAndCleanup(true)
    end

    local weiShu = math.floor(math.log10(fenShu) + 1)
    if weiShu == 1 and fenShu >= 3 then
        self.ttf:setFontSize(100)
        self.ttf:setPosition(ccp(jiLuSpSize.width*0.5 , jiLuSpSize.height*0.4))
    elseif weiShu == 2 then
        self.ttf:setFontSize(60)
        self.ttf:setPosition(ccp(jiLuSpSize.width*0.5 , jiLuSpSize.height*0.45))
    elseif weiShu == 3 then
        self.ttf:setFontSize(40)
        self.ttf:setPosition(ccp(jiLuSpSize.width*0.5 , jiLuSpSize.height*0.48))
    elseif weiShu == 4 then
        self.ttf:setFontSize(30)
        self.ttf:setPosition(ccp(jiLuSpSize.width*0.5 , jiLuSpSize.height*0.48))
    else
        self.ttf:setFontSize(25)
        self.ttf:setPosition(ccp(jiLuSpSize.width*0.5 , jiLuSpSize.height*0.5))
    end

    self.sp:setPosition(ccp(zeroX + jiLuSpSize.width * j,zeroY + jiLuSpSize.height * i))
end