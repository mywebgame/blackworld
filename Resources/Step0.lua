function XinShouManager:step0()
	--初始化方块集合
	for i=1,HANG do
		fangKuaiManager.fangKuaiTable[i] = {}
		for j=1,LIE do
			fangKuaiManager.fangKuaiTable[i][j] = FangKuai()
			fangKuaiManager.fangKuaiTable[i][j]:new(i,j)
		end
	end

	--随机生成1和2
	fangKuaiManager.fangKuaiTable[3][2].fenShu = 1
	fangKuaiManager.fangKuaiTable[2][3].fenShu = 2

	--这里根据当前方块的位置,进行生成
	for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			if fk.fenShu ~= 0 then
				--这个位置有方块,生成方块
				fk:create()
			end
		end
	end
	
	gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep0_1)
end

function XinShouManager:jianCe_step0()
	local function jianCe()
		local fk1 = nil 
		local fk2 = nil
		for i=1,HANG do
			for j=1,LIE do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				if fk.fenShu ~= 0 then
					if fk1 == nil then
						fk1 = fk
					else
						fk2 = fk
					end
				end
			end
		end

		if fk1.X == fk2.X or fk1.Y == fk2.Y then
			return true
		end
		return false
	end

	if self.bu == 0 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep0_2)
	elseif self.bu == 1 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep0_3)
	elseif jianCe() then
			--完成了
			cclog("sdfdsf")
			gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep0_4)
			gameLayer.kuang:setTexture(CCTextureCache:sharedTextureCache():addImage("diKuang.png"))
			self.step = self.step + 1
			cclog("self.step = %d",self.step)
			self.bu = 0
	end
end