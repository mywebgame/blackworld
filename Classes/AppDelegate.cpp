#include "cocos2d.h"
#include "CCEGLView.h"
#include "AppDelegate.h"
#include "CCLuaEngine.h"
#include "SimpleAudioEngine.h"
#include "Lua_extensions_CCB.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "Lua_web_socket.h"
#endif

#include "SocketManager.h"
#include "cocos-ext.h"
#include "lua_cjson.h"
#include "lua_pb.h"

//rotobuf/Protobuf-Messages --lua_out=/Users/huxiaozhou/Desktop/MyFiles/cocos2d-x-2.2.2/projects/BlackDuck/protobuf/Protobuf-Messages-Complied /Users/huxiaozhou/Desktop/MyFiles/cocos2d-x-2.2.2/projects/BlackDuck/protobuf/Protobuf-Messages/*.proto
                                                                                                                                                                                                                                             


//  ./tolua++ -tCocos2d -o LuaCocos2d.cpp Cocos2d.pkg

using namespace CocosDenshion;

USING_NS_CC;
USING_NS_CC_EXT;

AppDelegate::AppDelegate()
{
    
}

AppDelegate::~AppDelegate()
{
    SimpleAudioEngine::end();
}

bool AppDelegate::applicationDidFinishLaunching()
{
    //创建 socket 管理器
    SocketManager::sharedManager();
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
    CCEGLView::sharedOpenGLView()->setDesignResolutionSize(640, 960, kResolutionShowAll);
    
    // turn on display FPS
    pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    // register lua engine
    CCLuaEngine* pEngine = CCLuaEngine::defaultEngine();
    CCScriptEngineManager::sharedManager()->setScriptEngine(pEngine);

    CCLuaStack *pStack = pEngine->getLuaStack();
    lua_State *tolua_s = pStack->getLuaState();
    tolua_extensions_ccb_open(tolua_s);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    pStack = pEngine->getLuaStack();
    tolua_s = pStack->getLuaState();
    tolua_web_socket_open(tolua_s);
#endif
    
    //开启json支持
    pStack = pEngine->getLuaStack();
    tolua_s = pStack->getLuaState();
    luaopen_cjson(tolua_s);
    
    //开启protobuf 支持
    pStack = pEngine->getLuaStack();
    tolua_s = pStack->getLuaState();
    luaopen_pb(tolua_s);
    
    //执行main函数
    std::string path = CCFileUtils::sharedFileUtils()->fullPathForFilename("lua/main/main.lua");
    pEngine->executeScriptFile(path.c_str());
    
    //传入SocketManager给lua
    pEngine->getLuaStack()->pushCCObject(SocketManager::sharedManager(), "SocketManager");
    pEngine->executeGlobalFunction("setSocketManager",1);
    
    //初始化游戏
    pEngine->executeGlobalFunction("doInit");

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->stopAnimation();

    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->startAnimation();

    SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
