//
//  Quene.h
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-23.
//
//

#ifndef __BlackDuck__Quene__
#define __BlackDuck__Quene__

#include "cocos2d.h"

class Quene:public cocos2d::CCObject {
private:
    cocos2d::CCArray * _arrayObjs;
public:
    ~Quene();
    Quene();
    static Quene * create();
    void push(cocos2d::CCObject * objToPush);
    void pop();
    int size();
    cocos2d::CCObject * front();
    cocos2d::CCArray * getArray();
};

#endif /* defined(__BlackDuck__Quene__) */
