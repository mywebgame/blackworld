//
//  SocketClient.h
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-19.
//
//

#ifndef __BlackDuck__SocketClient__
#define __BlackDuck__SocketClient__

#include "cocos2d.h"
#include "ByteBuffer.h"
#include "Quene.h"
#include <iostream>
#include <pthread.h>

using namespace std;

const int	SocketClient_WAIT_CONNECT = 0;
const int	SocketClient_OK = 1;
const int	SocketClient_DESTROY = 2;

class IOMessage;
class SocketClient {
private:
    //socket
    std::string _strIP;
    int _iPort;
    
    //发送线程
    bool _bThreadSendCreated;
    pthread_t _pthread_t_send;              //发送线程id
    
    //接收线程
    bool _bThreadRecvCreated;
    pthread_t _pthread_t_recv;
    
public:
    //state
    int m_iState;
    int m_iSocket;
    
    //send and receive quene
    Quene * m_sendMsgQuene;
    Quene * m_recvMsgQuene;
    
    //发送接收队列同步锁
	pthread_mutex_t m_sendqueue_mutex;
    pthread_mutex_t m_recvqueue_mutex;
    
    //发送和接收缓冲区，发送缓冲区满的时候，会断开连接，并提示信号不好
    ByteBuffer m_cbRecvBuf;
    ByteBuffer m_cbSendBuf;
    
    //线程状态量
    pthread_cond_t m_thread_msg_cond;
    
    pthread_mutex_t m_thread_msg_mutex;     //发送线程互斥对象
public:
    ~SocketClient();
    SocketClient(std::string strIP,int portNum);
    
    static void * ThreadSendMessage(void *p);
    static void * ThreadReceiveMessage(void *p);
public:
    bool connectServer();
    void startSendThread();
    IOMessage * constructMessage(const char * data,int commandId);
    void sendMessage(IOMessage * msgSend);
};

#endif /* defined(__BlackDuck__SocketClient__) */
