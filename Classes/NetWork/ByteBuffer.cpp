//
//  ByteBuffer.cpp
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-19.
//
//

#include "ByteBuffer.h"
USING_NS_CC;

ByteBuffer::~ByteBuffer(){
    CC_SAFE_DELETE_ARRAY(buffer);
}

ByteBuffer::ByteBuffer(int capacity){
    buffer=new char[capacity];
    position=0;
    this->capacity=capacity;
    this->limit=capacity;
}

ByteBuffer::ByteBuffer(char* data,int offset,int size){
    
	buffer = new char[size];
	position = 0;
	memcpy(buffer,data+offset,size);
	this->capacity = size;
	this->limit = this->capacity;
}

int ByteBuffer::bytesToInt(unsigned char * bytes)
{
    
    int addr = bytes[3] & 0xFF;
    
    addr |= ((bytes[2] << 8) & 0xFF00);
    
    addr |= ((bytes[1] << 16) & 0xFF0000);
    
    addr |= ((bytes[0] << 24) & 0xFF000000);
    
    return addr;
    
}

int ByteBuffer::remaining(){
    return limit - position;
}

void ByteBuffer::put(const char* bytes,int offset,int len){
    if (position + len > capacity) {
        printf("error -ByteBuffer::put(const char* bytes,int offset,int len)---position=%d,len=%d,capacity=%d\n",position,len,capacity);
        return;
    }
    memcpy(buffer+position, bytes, len);
    position+=len;
}

int ByteBuffer::getPosition(){
    return position;
}

void ByteBuffer::setPosition(int p){
    if(p > limit) {
		printf("error ByteBuffer::setPosition p> limit------------p=%d,limit=%d\n",p,limit);
	}
	position = p;
}

int ByteBuffer::getLimit(){
	return limit;
}

int ByteBuffer::getCapacity(){
	return this->capacity;
}

char* ByteBuffer::getBuffer(){
	return buffer;
}
char* ByteBuffer::toByteArray(){
	char* tmp = new char[position];
	memcpy( tmp,buffer,position);
	return tmp;
}

unsigned char ByteBuffer::getByte(){
	if(position + 1 > limit){
		printf("error ByteBuffer::getByte() position+1> limit------------position=%d,limit=%d\n",position,limit);
		return 0;
	}
	return buffer[position++];
}

void ByteBuffer::get(char* bytes,int size){
	get(bytes,0,size);
}

void ByteBuffer::get(char* bytes,int offset,int len){
	if(position + len > limit){
		memset(bytes+offset, 0, len );
		printf("error ByteBuffer::get(char* bytes,int offset,int len) position+len> limit------------position=%d,len=%d,limit=%d\n",position,len,limit);
		return;
	}
	memcpy(bytes+offset,buffer+position,len);
	position += len;
}

void ByteBuffer::clear(){
	position = 0;
	this->limit = capacity;
}

int ByteBuffer::getLength(int offset)
{
    int lengthPos = position+ offset;
    unsigned char * pos = new unsigned char[4];
    for(int i = 0;i < 4 ;i++)
    {
        pos[i] = buffer[lengthPos+i];
    }
    return ByteBuffer::bytesToInt(pos);
}

void ByteBuffer::getAsBytes(unsigned char * bytes)
{
    for(int i = 0 ; i < 4 ; i++){
		bytes[i] = buffer[position];
		position++;
	}
}

void ByteBuffer::flip(){
	this->limit = position;
	position = 0;
}

void ByteBuffer::compact(){
	if(position > 0){
		for(int i = position; i < limit ; i++){
			buffer[i-position] = buffer[i];
		}
	}
	position = limit - position;
	limit = this->capacity;
}

void ByteBuffer::rewind(){
	position = 0;
}






