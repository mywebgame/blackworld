//
//  IOMessage.cpp
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-22.
//
//

#include "IOMessage.h"
#include "ByteBuffer.h"
USING_NS_CC;

IOMessage::~IOMessage(){

}

IOMessage::IOMessage(){
    
}

IOMessage * IOMessage::create(){
    IOMessage * msg=new IOMessage();
    msg->autorelease();
    return msg;
}

int IOMessage::datalength(){
    return ByteBuffer::bytesToInt(length)+13;
}

int IOMessage::getCommondId(){
    return ByteBuffer::bytesToInt(commandId);
}

char * IOMessage::getData(){
    return data;
}




