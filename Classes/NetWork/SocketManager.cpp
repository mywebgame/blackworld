//
//  SocketManager.cpp
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-22.
//
//

#include "SocketManager.h"
#include "IOMessage.h"

USING_NS_CC;

static SocketManager* s_sharedSocketManager = NULL;
SocketManager * SocketManager::sharedManager(){
    if (!s_sharedSocketManager) {
        s_sharedSocketManager=new SocketManager();
    }
    return s_sharedSocketManager;
}

SocketManager::~SocketManager(){
    CC_SAFE_DELETE(_socketClient);
}

SocketManager::SocketManager(){
    _socketClient=new SocketClient("127.0.0.1",11009);
}

bool SocketManager::connectServer(){
    if (_socketClient->connectServer()) {
        _socketClient->startSendThread();
        return true;
    }
    return false;
}

void SocketManager::sendMessage(const char* data,int commandId){
    CCLog("sending:%d,%s",commandId,data);
    IOMessage * msgSend = _socketClient->constructMessage(data, commandId);
    _socketClient->sendMessage(msgSend);
}

Quene * SocketManager::getRecvQuene(){
    return _socketClient->m_recvMsgQuene;
}













