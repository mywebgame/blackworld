//
//  ThreadLock.h
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-23.
//
//

#ifndef __BlackDuck__ThreadLock__
#define __BlackDuck__ThreadLock__

#include <iostream>
#include <pthread.h>

class ThreadLock {
private:
    pthread_mutex_t * mutex_t;
public:
    ~ThreadLock();
    ThreadLock(pthread_mutex_t * mutexToLock);
};

#endif /* defined(__BlackDuck__ThreadLock__) */
