//
//  SocketManager.h
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-22.
//
//

#ifndef __BlackDuck__SocketManager__
#define __BlackDuck__SocketManager__

#include <iostream>
#include "cocos2d.h"
#include "SocketClient.h"
#include "Quene.h"

class SocketManager:public cocos2d::CCObject{
private:
    SocketClient * _socketClient;
public:
    ~SocketManager();
    SocketManager();
    
    static SocketManager * sharedManager();
    
    //methods
    bool connectServer();
    void sendMessage(const char* data,int commandId);
    Quene * getRecvQuene();
};

#endif /* defined(__BlackDuck__SocketManager__) */
