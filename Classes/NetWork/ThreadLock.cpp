//
//  ThreadLock.cpp
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-23.
//
//

#include "ThreadLock.h"
#include "cocos2d.h"
USING_NS_CC;

ThreadLock::~ThreadLock(){
    pthread_mutex_unlock(this->mutex_t);
}

ThreadLock::ThreadLock(pthread_mutex_t * mutexToLock){
    this->mutex_t = mutexToLock;
    pthread_mutex_lock(this->mutex_t);
}