//
//  ByteBuffer.h
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-19.
//
//

#ifndef __BlackDuck__ByteBuffer__
#define __BlackDuck__ByteBuffer__

#include <iostream>
#include "cocos2d.h"

class ByteBuffer {
public:
public:
	char * buffer;
	int capacity;
    int position;
    int limit;
	int mark;
public:
    ~ByteBuffer();
    ByteBuffer(int capacity);
    ByteBuffer(char* data,int offset,int length);
    static int bytesToInt(unsigned char * bytes);
    
    
    int remaining();
    void put(const char* bytes,int offset,int len);
    int getPosition();
	void setPosition(int p);
    int getLimit();
	int getCapacity();
    
    char * getBuffer();
	char * toByteArray();
    
    unsigned char getByte();
    void get(char* bytes,int size);
	void get(char* bytes,int offset,int len);
    
    void clear();
    int getLength(int offset);
    void getAsBytes(unsigned char * bytes);
    
    //设置limit到当前位置(position)，然后使当前位置(position)置0
    void flip();
    //紧凑
    void compact(); 
    void rewind();
};

#endif /* defined(__BlackDuck__ByteBuffer__) */












