require "AudioEngine" 
require "lua/utils/utils"
require "lua/Layers/Layer_Login"

-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)
    print("----------------------------------------")
    print("LUA ERROR: " .. tostring(msg) .. "\n")
    print(debug.traceback())
    print("----------------------------------------")
end

--------- SocketManager
local socketManager = nil

--------- SocketManager set
function setSocketManager(sManager)
    socketManager = sManager;
end

local function main()
    -- avoid memory leak
    collectgarbage("setpause", 100)
    collectgarbage("setstepmul", 5000)

    ---------------
    local sceneGame = CCScene:create()
    sceneGame:addChild(loginLayer())
    CCDirector:sharedDirector():runWithScene(sceneGame)
end

xpcall(main, __G__TRACKBACK__)
